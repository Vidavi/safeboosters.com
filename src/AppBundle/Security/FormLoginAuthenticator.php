<?php
namespace AppBundle\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use AppBundle\Entity\Log;
 use Symfony\Component\DependencyInjection\ContainerInterface;
class FormLoginAuthenticator extends AbstractFormLoginAuthenticator
{
    private $router;
    private $encoder;
    private $Session;
    private $Container;
    public function __construct(RouterInterface $router, UserPasswordEncoderInterface $encoder, Session $Session, ContainerInterface $Container)
    {
        $this->router = $router;
        $this->encoder = $encoder;
        $this->Session = $Session;
        $this->Container = $Container;
    }

    public function getCredentials(Request $request)
    {
        if ($request->getPathInfo() != '/login_check') {
          return;
        }

        $email = $request->request->get('_email');
        $request->getSession()->set(Security::LAST_USERNAME, $email);
        // $request->getSession()->set('ID', 2);
        $password = $request->request->get('_password');

        return [
            'email' => $email,
            'password' => $password,
        ];
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $email = $credentials['email'];

        return $userProvider->loadUserByUsername($email);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        $plainPassword = $credentials['password'];
        if ($this->encoder->isPasswordValid($user, $plainPassword)) {


           //   
           //  $session->set('user', array('id' => $user->getID(), "email" => $user->getEmail()));
           
          
       
            return true;
        }

        throw new BadCredentialsException();
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {

        $user = $token->getUser();
        $this->Session->set('user/id', $user->getID());
        $this->Session->set('user/email', $user->getEmail());

        // USER DETAILS
        $action = "Login";
        $helper = $this->Container->get('helper.functions');
        $helper-> log($user->getID(), $action);

        $trial = $this->Container->getParameter('trial');
        if($user->getTrial() == 0){
            $date = new \DateTime('now');
            $date->modify("+ ".$trial. " days");

    
            $helper->activeTrial($user->getID(), $date);
        }

        $url = $this->router->generate('activity');

        return new RedirectResponse($url);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
       $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);

       $url = $this->router->generate('login');

       return new RedirectResponse($url);
    }

    protected function getLoginUrl()
    {
        return $this->router->generate('login');
    }

    protected function getDefaultSuccessRedirectUrl()
    {

        return $this->router->generate('welcome');
    }

    public function supportsRememberMe()
    {
        return false;
    }
}