<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class EmailsType extends AbstractType{

		public function buildForm(FormBuilderInterface $builder, array $options){
	
			 $builder
    	   			->add('email', EmailType::class, array('attr' => array('placeholder' => 'New email')));
	
		}

		public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults([
            'error_bubbling' => false
         
        ]);
    }

	public function getName()
    {
        return 'email_form';
    }
}
