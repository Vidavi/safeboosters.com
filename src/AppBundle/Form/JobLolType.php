<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class JobLolType extends AbstractType
{
   public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', TextType::class, array('attr' => array('style'=>'display:none;'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Invalid form")),
                ) 
            ))
            ->add('skype', TextType::class, array('attr' => array('placeholder' => 'Your Skype *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Please provide your Skype")),
                ) 
            ))
            ->add('email', EmailType::class, array('attr' => array('placeholder' => 'Your email address'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Please provide a valid email")),
                    new Email(array("message" => "Your email doesn't seems to be valid")),
                )
            ))
            ->add('summoner', TextareaType::class, array('attr' => array('placeholder' => 'Your Summoner name or op.gg profile *vvvvv'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Please provide a Summoner name")),
                )
            ))
            ->add('server', TextareaType::class, array('attr' => array('placeholder' => 'Server *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Please provide your platform location")),
                )
            ))
            ->add('roles', TextareaType::class, array('attr' => array('placeholder' => 'Roles you play *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Please provide your main role/s")),
                )
            ))
            ->add('laguage', TextareaType::class, array('attr' => array('placeholder' => 'Laguages you speak *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Please provide laguages you speak")),
                )
            ))
            ->add('age', TextareaType::class, array('attr' => array('placeholder' => 'How old are you *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Please provide your age")),
                )
            ))
            ->add('persent', TextareaType::class, array('attr' => array('placeholder' => 'How much of the order price would you like be happy to start working and why?*'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Please provide for how much would you like be happy to start working")),
                )
            ))
            ->add('hours', TextareaType::class, array('attr' => array('placeholder' => 'Hours awaliable per day *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Please provide your awaliable time per day")),
                )
            ))
            ->add('coach', TextareaType::class, array('attr' => array('placeholder' => 'Can you coach? *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Please provide coach information")),
                )
            ))
            ->add('message', TextareaType::class, array('attr' => array('placeholder' => 'Your message here'), 'required' => true,
                'constraints' => array(
                )
            ))

        ;
    }

    public function getName()
    {
        return 'job_form';
    }
}