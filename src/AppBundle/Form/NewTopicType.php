<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\length;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\FileValidator;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\Image;

class NewTopicType extends AbstractType
{
   public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('topic_name', TextType::class, array('attr' => array('placeholder' => 'Topic name'), 'required' => true,
                'constraints' => array(
                    new length(array("min" => 3, "max"=> 30)),
                    new NotBlank(array("message" =>  "Topic name cannot be blank")),
                ) 
            ))
            ->add('scamer_name', TextType::class, array('attr' => array('placeholder' => 'Scammer nickname'), 'required' => true,
                'constraints' => array(
                    new length(array("max"=> 30)),
                    new NotBlank(array("message" => "Please provide name or nickname")),
                ) 
            ))
            ->add('scammer_IGN', TextType::class, array('attr' => array('placeholder' => 'Scammer IGN'), 'required' => false,
                'constraints' => array(
                    new length(array("min" => 3, "max"=> 30))
                )
            ))
            ->add('scammer_email', EmailType::class, array('attr' => array('placeholder' => 'Scammer email address'), 'required' => false,
                'constraints' => array(
                    new Email(array("message" => "Invalid email address"))
                )
            ))
            ->add('scammer_skype', TextType::class, array('attr' => array('placeholder' => 'Scammer skype'), 'required' => false,
                'constraints' => array(
                    new length(array("min" => 3, "max"=> 30))
                )
            ))
            ->add('scammer_contactApp', TextType::class, array('attr' => array('placeholder' => 'Scammer another than skype messaging app'), 'required' => false,
                'constraints' => array(
                    new length(array("min" => 3, "max"=> 30))
                )
            ))
            ->add('story', TextareaType::class, array('attr' => array('placeholder' => 'Your message', 'class' => 'tinymce' ,'id' => 'ed1here'),
                'constraints' => array(
                    new length(array("min" => 10, "max"=> 3500)),
                    new NotBlank(array('message' => 'Description should not be blank.')),
                )
            ))
            ->add('brochure', FileType::class, array('label' => 'Brochure (PDF file)', 'multiple' => true,'required' => false,
                   
                    'constraints' => array(
                     new All(array(
            'constraints' => array(
                new Image(array(
                    'maxSize'       => '1M'


                ))
            )
        ))
                )
        ));
    }

    public function getName()
    {
        return 'new_topic_form';
    }
}