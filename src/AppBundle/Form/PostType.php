<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\NotBlank;

class PostType extends AbstractType
{
   public function buildForm(FormBuilderInterface $builder, array $options) 
    {
        $builder
            ->add('post_content', TextareaType::class, array('attr' => array('placeholder' => 'Your message', 'class' => 'tinymce' ,'id' => 'ed1here'),
                'constraints' => array(
                   
                )
            ))
         
        ;
    }

    public function getName()
    {
        return 'contact_form';
    }
}