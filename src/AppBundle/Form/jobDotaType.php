<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class jobDotaType  extends AbstractType
{
   public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', TextType::class, array('attr' => array('style'=>'display:none;'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Invalid form")),
                ) 
            ))
            ->add('skype', TextType::class, array('attr' => array('placeholder' => 'Your Skype'), 'required' => false,
                'constraints' => array(
                    new NotBlank(array("message" => "Please provide your Skype")),
                ) 
            ))
            ->add('email', EmailType::class, array('attr' => array('placeholder' => 'Your email address*'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Please provide a valid email")),
                    new Email(array("message" => "Your email doesn't seems to be valid")),
                )
            ))
            ->add('discord', TextType::class, array('attr' => array('placeholder' => 'Your Discord'), 'required' => false,
                'constraints' => array(
                    new NotBlank(array("message" => "Please provide your Discord")),
                ) 
            ))
            ->add('fullName', TextareaType::class, array('attr' => array('placeholder' => 'Your Full Name *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Please provide a Full Name")),
                )
            ))
            ->add('steamId', TextareaType::class, array('attr' => array('placeholder' => 'Your Steam ID *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Your Steam ID? ")),
                )
            ))
            ->add('whereAreYouFrom', TextareaType::class, array('attr' => array('placeholder' => 'Where are you from? *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Please provide where are you from")),
                )
            ))
            ->add('server', TextareaType::class, array('attr' => array('placeholder' => 'Servers you can play(EU/USA/SEA/Australia/China) *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Servers you can play(EU/USA/SEA/Australia/China) *")),
                )
            ))
            ->add('age', TextareaType::class, array('attr' => array('placeholder' => 'How old are you *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Please provide your age")),
                )
            ))
            ->add('rank', TextareaType::class, array('attr' => array('placeholder' => 'Your current Rank *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Your current Rank *")),
                )
            ))
            ->add('hours', TextareaType::class, array('attr' => array('placeholder' => 'How much time you can dedicate to that work? *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Please provide how much time you can dedicate to that work *")),
                )
            ))
            ->add('highestRankaccountScreenShot', TextareaType::class, array('attr' => array('placeholder' => 'Highest screen shot of your account imgur.com) *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Please provide Screenshot of your account in CS:GO client (imgur.com)")),
                )
            ))
            ->add('specs', TextareaType::class, array('attr' => array('placeholder' => 'Your computer specs *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Please provide your computer specs")),
                )
            ))
            ->add('stream', TextareaType::class, array('attr' => array('placeholder' => 'Can you stream your orders(on private stream)? *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Please provide streamning infomation")),
                )
            ))

            ->add('DuofromOwnAccount', TextareaType::class, array('attr' => array('placeholder' => 'Can you play DuoQ(on your own accounts)? *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Can you play DuoQ(on your own accounts)? *")),
                )
            ))
            ->add('serviceType', TextareaType::class, array('attr' => array('placeholder' => 'What kind of service you can handle(coach/lvling)? *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "What kind of service you can handle(coach/lvling)? *")),
                )
            ))
            ->add('abilityToUseEnglish', TextareaType::class, array('attr' => array('placeholder' => 'Are you fluent in Enlish? *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Are you fluent in Enlish? *")),
                )
            ))
            ->add('workOrStudy', TextareaType::class, array('attr' => array('placeholder' => 'Are you working or studying now? *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Are you working or studying now? *")),
                )
            ))
            ->add('laguage', TextareaType::class, array('attr' => array('placeholder' => 'Laguages you speak? *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Please provide aguages that you speak")),
                )
            ))
            ->add('testBoost', TextareaType::class, array('attr' => array('placeholder' => 'Will you do a first free boost to prove your worth? *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Will you do a first free boost to prove your worth? *")),
                )
            ))
            ->add('persent', TextareaType::class, array('attr' => array('placeholder' => 'How much of the order price would you like be happy to start working and why?*'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Please provide for how much would you like be happy to start working")),
                )
            ))
            ->add('stableInternet', TextareaType::class, array('attr' => array('placeholder' => 'Do you have stable internet? *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Do you have stable internet?")),
                )
            ))
            ->add('boosterExperience', TextareaType::class, array('attr' => array('placeholder' => 'Did you work as a booster before? *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Did you work as a booster before?")),
                )
            ))
            ->add('accountScreenShot', TextareaType::class, array('attr' => array('placeholder' => 'Screenshot of your account showing Rank and Name (imgur.com) *'), 'required' => true,
                'constraints' => array(
                    new NotBlank(array("message" => "Screenshot of your account showing Rank and Name(imgur.com)")),
                )
            ))
           
            ->add('message', TextareaType::class, array('attr' => array('placeholder' => 'Why should we hire you?*'), 'required' => true,
                'constraints' => array(
                )
            ))

        ;
    }

    public function getName()
    {
        return 'OverwatchForm';
    }
}