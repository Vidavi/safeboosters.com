<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\IsTrue;
class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array('label' => false,  'attr' => array('placeholder' => 'name', 'class' =>'form-control')))
            ->add('email', EmailType::class, array('label' => false,  'attr' => array('placeholder' => 'email', 'class' =>'form-control')))
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => ['label' => false, 'attr' => array('placeholder' => 'password', 'class' =>'form-control')],
                'second_options' => ['label' => false, 'attr' => array('placeholder' => 'confirm password', 'class' =>'form-control')],
            ])
            ->add('terms', CheckboxType::class, ['required' => true,
                'constraints' => new IsTrue(array("message" => "Please accept terms of service")),
            ]);
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\User',
        ]);
    }
}