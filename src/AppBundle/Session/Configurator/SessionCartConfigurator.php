<?php
 
namespace AppBundle\Session\Configurator;
 
use Pimcore\Session\SessionConfiguratorInterface;
use Symfony\Component\HttpFoundation\Session\Attribute\NamespacedAttributeBag;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
 
class SessionCartConfigurator implements SessionConfiguratorInterface
{
    /**
     * @inheritDoc
     */
    public function configure(SessionInterface $session)
    {
        $bag = new NamespacedAttributeBag('_session_cart');
        $bag->setName('session_cart');
 
        $session->registerBag($bag);
    }
}