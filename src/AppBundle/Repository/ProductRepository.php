<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
class ProductRepository extends EntityRepository
{
     public function findById($id)
    {
    
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
          ->select('p')
          ->from('AppBundle:Product', 'p')
          ->where('p.userId = :id')
          ->setParameter('id', $id)
          ->setMaxResults(1);
        try {
              return $qb->getQuery()->getArrayResult();


        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
   
    
}

