<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
class logRepository extends EntityRepository
{
     public function lastlogs()
    {
    
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
          ->select('l')
          ->from('AppBundle:log', 'l')
          ->orderBy('l.id', 'DESC')
          ->setMaxResults(300);
        try {
              return $qb->getQuery()->getResult(); 


        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    public function unverified()
    {
    
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
          ->select('p', 't')
                ->from('AppBundle:Topic' ,'t')
          ->from('AppBundle:Post' ,'p')
          
         
            
                 ->Where("p.checkk = :check")
          ->setParameter("check",false);
    
    
        try {
              return $qb->getQuery()->getResult(); 


        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
}

