<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class forumRepository extends EntityRepository
{
    public function findCategoryById($cat_id)
    {
    	 $query = $this->getEntityManager()->createQuery(
        'SELECT c FROM AppBundle:Category c
        WHERE c.id = :cat_id')
    	 ->setParameter('cat_id', $cat_id);
    

    	try {
    	    return $query->getSingleResult();
    	} catch (\Doctrine\ORM\NoResultException $e) {
    	    return null;
    	}

    }
    public function findTopicById($topic_id)
    {
     

         $qb = $this->getEntityManager()->createQueryBuilder();
          $qb

         ->select('t', 'c')
          ->from('AppBundle:Topic', 't')
          ->innerjoin('AppBundle:Category', 'c', 'WITH', 't.topic_cat = c.id')
          ->where('t.id = :topic_id')

          ->setParameter('topic_id', $topic_id)
       
           ->setMaxResults(1);

        try {
            return $qb->getQuery()->getResult(); // getOneOrNullResult()
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

    }

    public function findPostById($post_id)
    {
     

         $qb = $this->getEntityManager()->createQueryBuilder();
          $qb

         ->select('p')
          ->from('AppBundle:Post', 'p')
          ->where('p.id = :post_id')

          ->setParameter('post_id', $post_id)
       
           ->setMaxResults(1);

        try {
            return $qb->getQuery()->getSingleResult(); // getOneOrNullResult()
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

    }

    public function findAllTopicById($cat_id)
    {


 $qb = $this->getEntityManager()->createQueryBuilder();
          $qb

       ->select('t')
          ->from('AppBundle:Topic', 't')
        
          ->where('t.topic_cat = :categoryId')

          ->setParameter('categoryId', $cat_id)
          ->orderBy('t.Created_at', 'DESC');
       


    

      try {
          return $qb->getQuery()->getResult(); // getOneOrNullResult()
      } catch (\Doctrine\ORM\NoResultException $e) {
          return null;
      }

    }

    public function findAllPostById($post_topic)
    {

      
            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb
            ->select('p')
          ->from('AppBundle:Post', 'p')
          ->where('p.post_topic = :post_topic')
          ->setParameter('post_topic', $post_topic)
          ->orderBy('p.Created_at', 'DESC');
try {
            return $qb->getQuery()->getResult(); // getOneOrNullResult()
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    public function findTopicByDate($categoryId)
    {
      //    $query = $this->getEntityManager()->createQuery(
      //   'SELECT t FROM AppBundle:Topic t
      //   WHERE t.topic_cat = :categoryId
      //   ORDER BY t.Created_at DESC ')
      //    ->setParameter('categoryId', $categoryId)
      //   ->setMaxResults(1);
         $qb = $this->getEntityManager()->createQueryBuilder();
          $qb
      //      ->select('t')
      //  ->from('AppBundle:Topic', 't')
      //  //->leftJoin('a.user', 'u')
      //  ->where('t.topic_cat = :categoryId')
      //  ->setParameter('categoryId', $categoryId)
      //  ->orderBy('t.Created_at', 'DESC')
      //   ->setMaxResults(1);
         ->select('t', 'u')
          ->from('AppBundle:Topic', 't')
          ->innerjoin('AppBundle:User', 'u', 'WITH', 't.topic_by = u.id')
          ->where('t.topic_cat = :categoryId')

          ->setParameter('categoryId', $categoryId)
          ->orderBy('t.Created_at', 'DESC')
           ->setMaxResults(1);

        try {
            return $qb->getQuery()->getResult(); // getOneOrNullResult()
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

    }

      public function search($info)
    {
      
         $qb = $this->getEntityManager()->createQueryBuilder();
          $qb
          ->select('t')
          ->from('AppBundle:Topic', 't')
          ->where('t.scamer_name LIKE :name')


          ->orWhere('t.scamer_name LIKE :name')
          ->setParameter('name', $info['scamer_name']);

        
          // ->orWhere('t.scammer_email LIKE :scammer_email')
          // ->orWhere('t.scammer_skype LIKE :scammer_skype')
          // ->orWhere('t.scammer_contact_app LIKE :scammer_contact_app')


          if(isset($info['scammer_email'])){
              $qb->orWhere('t.scammer_IGN LIKE :IGN')
             ->setParameter('IGN', $info['scammer_ign']);
          }

          
          // ->setParameter('scammer_email', $info['scammer_email'])
          // ->setParameter('scammer_skype', $info['scammer_skype'])
          // ->setParameter('scammer_contact_app', $info['scammer_contact_app']);

  if(isset($info['scammer_email'])){
              $qb->andWhere('t.scammer_email = :jezyk');
              $qb->setParameter('scammer_email',  $info['scammer_email']);  
          }
               try {
            return $qb->getQuery()->getResult(); // getOneOrNullResult()
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

     public function findAllTopic()
    {


 $qb = $this->getEntityManager()->createQueryBuilder();
          $qb

       ->select('t')
          ->from('AppBundle:Topic', 't');
      try {
          return $qb->getQuery()->getResult(); // getOneOrNullResult()
      } catch (\Doctrine\ORM\NoResultException $e) {
          return null;
      }

    }
     public function checkPostById($topicId, $userId)
    {
        
            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb
            ->select('p')
          ->from('AppBundle:Post', 'p')
          ->where('p.post_topic = :post_topic')
          ->andWhere('p.post_by = :post_by')
          ->setParameter('post_topic', $topicId)
          ->setParameter('post_by', $userId)
          ->orderBy('p.Created_at', 'DESC');
    
            return $qb->getQuery()->getResult(); // getOneOrNullResult()
        
    }
    public function unverifiedTopic()
    {
    
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('t')
            ->from('AppBundle:Topic' ,'t')
           ->Where("t.status = :status")
          ->setParameter("status",false);
    
    
     
              return $qb->getQuery()->getArrayResult();


    }
     public function unverifiedPost()
    {
    
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('p')
            ->from('AppBundle:Post' ,'p')
           ->Where("p.status = :status")
          ->setParameter("status",false);
    
    
     
              return $qb->getQuery()->getArrayResult();


    }
   
}

