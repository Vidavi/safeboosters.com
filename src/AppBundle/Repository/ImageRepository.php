<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\ArrayCollection;

class ImageRepository extends EntityRepository
{
     public function loadfilesById($id)
    {
    
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
          ->select('i')
          ->from('AppBundle:Image', 'i')
          ->where('i.fileId = :id')
          ->setParameter('id', $id);

        try {
              return $qb->getQuery()->getResult();


        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
     public function loadAvatarById($id)
    {
    
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
          ->select('a')
          ->from('AppBundle:Avatar', 'a')
          ->where('a.userId = :id')
          ->setParameter('id', $id);

        try {
              return $qb->getQuery()->getOneOrNullResult();


        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    public function getImageById($id)
    {
    
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
          ->select('i')
          ->from('AppBundle:Image', 'i')
          ->where('i.id = :id')
          ->setParameter('id', $id);

   
              return $qb->getQuery()->getOneOrNullResult();

    }
    
     public function loadfilesByIdJson($id)
    {
    
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
          ->select('i')
          ->from('AppBundle:Image', 'i')
          ->where('i.fileId = :id')
          ->setParameter('id', $id);

        try {
             return $qb->getQuery()->getArrayResult();


        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
   
    
}

