<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class TopicRepository extends EntityRepository
{
    public function findAllTopicById($cat_id)
    {
    	 $query = $this->getEntityManager()->createQuery(
        'SELECT t FROM AppBundle:Topic t
        WHERE t.topic_cat = :topic_cat')
    	 ->setParameter('topic_cat', $cat_id);
    

    	try {
    	    return $query->getResult();
    	} catch (\Doctrine\ORM\NoResultException $e) {
    	    return null;
    	}

    }
    public function findTopicByDate($categoryId)
    {
      //    $query = $this->getEntityManager()->createQuery(
      //   'SELECT t FROM AppBundle:Topic t
      //   WHERE t.topic_cat = :categoryId
      //   ORDER BY t.Created_at DESC ')
      //    ->setParameter('categoryId', $categoryId)
      //   ->setMaxResults(1);
         $qb = $this->getEntityManager()->createQueryBuilder();
          $qb
      //      ->select('t')
      //  ->from('AppBundle:Topic', 't')
      //  //->leftJoin('a.user', 'u')
      //  ->where('t.topic_cat = :categoryId')
      //  ->setParameter('categoryId', $categoryId)
      //  ->orderBy('t.Created_at', 'DESC')
      //   ->setMaxResults(1);
         ->select('t', 'u')
          ->from('AppBundle:Topic', 't')
          ->innerjoin('AppBundle:User', 'u', 'WITH', 't.topic_by = u.id')
          ->where('t.topic_cat = :categoryId')

          ->setParameter('categoryId', $categoryId)
          ->orderBy('t.Created_at', 'DESC')
           ->setMaxResults(1);

        try {
            return $qb->getQuery()->getResult(); // getOneOrNullResult()
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

    }
}

