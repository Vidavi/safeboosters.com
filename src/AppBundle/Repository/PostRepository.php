<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class PostRepository extends EntityRepository
{
    public function findAllPostById($post_topic)
    {
    	// $query = $this->getEntityManager()->createQuery(
       // 'SELECT t FROM AppBundle:Post t
       // WHERE t.post_topic = :topic_cat')
    	// ->setParameter('topic_cat', $post_topic);
    //
//
    	//try {
    	//    return $query->getResult();
    	//} catch (\Doctrine\ORM\NoResultException $e) {
    	//    return null;
    	//}
//

            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb
            ->select('p')
          ->from('AppBundle:Post', 'p')
          ->where('p.post_topic = :post_topic')
          ->setParameter('post_topic', $post_topic)
          ->orderBy('p.Created_at', 'DESC');
try {
            return $qb->getQuery()->getResult(); // getOneOrNullResult()
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}

