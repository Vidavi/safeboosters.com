<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class PaymentRepository extends EntityRepository
{
    public function txn_id($txn_id)
    {
    	 $qb = $this->getEntityManager()->createQueryBuilder();
          $qb

          ->select('p')
          ->from('AppBundle:Paypal', 'p')
          ->where('p.txn_id = :txn_id')
          ->setParameter('txn_id', $txn_id)
          ->setMaxResults(1);

        try {
            return $qb->getQuery()->getOneOrNullResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

    }
   
    
}

