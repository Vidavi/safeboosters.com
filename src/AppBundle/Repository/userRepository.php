<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
class userRepository extends EntityRepository
{
     public function getUSerById($userId)
    {
    
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
          ->select('u')
          ->from('AppBundle:User', 'u')
          ->where('u.id = :userId')
          ->setParameter('userId', $userId)
          ->setMaxResults(1);
        try {
              return $qb->getQuery()->getOneOrNullResult(); 


        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    public function getAllUsers()
    {
    
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
          ->select('u')
          ->from('AppBundle:User', 'u');
   
               try {
              return $qb->getQuery()->getArrayResult();


        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    public function checkEmail($email)
    {
    
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
          ->select('u')
          ->from('AppBundle:User', 'u')
          ->where('u.email = :email')
          ->setParameter('email', $email)
          ->setMaxResults(1);
        try {
              return $qb->getQuery()->getOneOrNullResult(); 


        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
public function getTokens()
    {
    
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
          ->select('t')
          ->from('AppBundle:RegisterToken', 't');
        try {
              return $qb->getQuery()->getArrayResult();


        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    public function checkToken($token)
    {
    
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
          ->select('t')
          ->from('AppBundle:RegisterToken', 't')
          ->where('t.token = :token')
          ->setParameter('token', $token)
          ->setMaxResults(1);
        try {
              return $qb->getQuery()->getOneOrNullResult();


        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
   
    
}

