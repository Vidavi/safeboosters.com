<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
class newsRepository extends EntityRepository
{
     public function news()
    {
      
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('n', 'u', 't', 'j', 'p')
            ->from('AppBundle:News', 'n')
            ->leftJoin('AppBundle:Topic', 't', 'WITH', 'n.topic_id = t.id')
            ->leftJoin('AppBundle:Job', 'j', 'WITH', 'n.job_id = j.id')
            ->leftJoin('AppBundle:Post', 'p', 'WITH', 'n.postId = p.id')
            ->leftJoin('AppBundle:user', 'u', 'WITH', 'n.user_id = u.id')
            ->orderBy('n.id', 'DESC')
            ->setMaxResults(10);
     
        try {
                  //   return $qb->getQuery()->getResult(); // getOneOrNullResult()
            return $qb->getQuery()->getScalarResult(); //getResult(\Doctrine\ORM\Query::HYDRATE_SCALAR);
                  // return $qb->getQuery()->getScalarResult();
                  // return     $qb->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_OBJECT);
                  // $collection = new ArrayCollection($qb->getQuery()->getResult());

        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function profileNews($userId)
    {
      
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('n', 'u', 't', 'p')
            ->from('AppBundle:News', 'n')
            ->leftJoin('AppBundle:Topic', 't', 'WITH', 'n.topic_id = t.id')
            ->leftJoin('AppBundle:Post', 'p', 'WITH', 'n.topic_id = p.post_topic')
            ->leftJoin('AppBundle:user', 'u', 'WITH', 'n.user_id = u.id')
            ->where('n.user_id = :userId')
            ->setParameter('userId', $userId)
            ->setMaxResults(5)
            ->orderBy('n.id', 'DESC');
 
        try {
                  //   return $qb->getQuery()->getResult(); // getOneOrNullResult()
            return $qb->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_SCALAR);
                  // return $qb->getQuery()->getScalarResult();
                  // return     $qb->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_OBJECT);
                  // $collection = new ArrayCollection($qb->getQuery()->getResult());

        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}

