<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
class ContactRepository extends EntityRepository
{
     public function jobById($id)
    {
    
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
          ->select('j')
          ->from('AppBundle:Job', 'j')
          ->where('j.id = :id')
          ->setParameter('id', $id)
          ->setMaxResults(1);
        try {
              return $qb->getQuery()->getArrayResult();


        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    public function unverifiedPJob()
    {
    
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('j')
            ->from('AppBundle:Job' ,'j')
           ->Where("j.status = :status")
          ->setParameter("status",false);
    
    
     
              return $qb->getQuery()->getArrayResult();


    }
     public function jobByIdObject($id)
    {
    
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
          ->select('j')
          ->from('AppBundle:Job', 'j')
          ->where('j.id = :id')
          ->setParameter('id', $id)
          ->setMaxResults(1);
   
              return $qb->getQuery()->getSingleResult();


      
    }
    
   
    
}

