<?php
namespace AppBundle\helper;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\Log;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Session\Session; 

class CustomFunctions {

    private $entityManager;
   // private $ContainerInterface;
    private $Session;

    public function __construct(EntityManager $entityManager, Session $Session) {
        $this->entityManager = $entityManager;
         $this->Session = $Session;
        //$this->ContainerInterface = $ContainerInterface;
    }

    public function log($id = null, $action, $description = null) {
	        // USER DETAILS
        $ip = $_SERVER['REMOTE_ADDR'];
        $browser = $_SERVER['HTTP_USER_AGENT'];
        $referrer = $_SERVER['HTTP_REFERER'];

        $log = new Log();

        $log->setDetails($description);
        $log->setUserId($id);
        $log->setAction($action);
        $log->setReferrer($referrer);
        $log->setBrowser($browser);
        $log->setIp($ip);

       $this->entityManager->persist($log);
        $this->entityManager->flush();
    }
    public function activeTrial($userId, $date) {
            
    
        $user = $this->entityManager->getRepository('AppBundle:User')->getUserById($userId);
       
        $user->setTrial(1);
        $user->setSubEndDate($date);
        try {
            $this->entityManager->getConnection()->beginTransaction();
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            $this->entityManager->getConnection()->commit();
        } catch (\Exception $e) {
            $this->entityManager->getConnection()->rollback();
            throw $e;
        }

    }
}