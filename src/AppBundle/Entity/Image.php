<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ImageRepository")
 * @ORM\Table(name="Image")
 */

class Image
{
    /**
 * @ORM\Id
 * @ORM\Column(type="integer")
 * @ORM\GeneratedValue(strategy="AUTO")
 */
private $id;


    /**

     * @ORM\Column(name="brochure", type="array", nullable=true)

     */
    private $brochure;
    /**
    * @ORM\Column(type="string")
    */
    protected $imagePath;
    /**
    * @ORM\Column(type="string")
    */
    protected $imageOriginalName;
    /**
    * @ORM\Column(type="string")
    */
    protected $fileId;
     /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $createdAt;

   

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set brochure
     *
     * @param array $brochure
     *
     * @return Image
     */
    public function setBrochure($brochure)
    {
        $this->brochure = $brochure;

        return $this;
    }

    /**
     * Get brochure
     *
     * @return array
     */
    public function getBrochure()
    {
        return $this->brochure;
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     *
     * @return Image
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * Set imageOriginalName
     *
     * @param string $imageOriginalName
     *
     * @return Image
     */
    public function setImageOriginalName($imageOriginalName)
    {
        $this->imageOriginalName = $imageOriginalName;

        return $this;
    }

    /**
     * Get imageOriginalName
     *
     * @return string
     */
    public function getImageOriginalName()
    {
        return $this->imageOriginalName;
    }

    /**
     * Set fileId
     *
     * @param string $fileId
     *
     * @return Image
     */
    public function setFileId($fileId)
    {
        $this->fileId = $fileId;

        return $this;
    }

    /**
     * Get fileId
     *
     * @return string
     */
    public function getFileId()
    {
        return $this->fileId;
    }

    /**
     * Set createdAt
     *
     * @param string $createdAt
     *
     * @return Image
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
