<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContactRepository")
 * @ORM\Table(name="job")
@ORM\HasLifecycleCallbacks
*/
class Job
{

		/**
		* @ORM\Id;
		* @ORM\Column(type="integer")
		* @ORM\GeneratedValue(strategy="AUTO")
		*/
		protected $id;
		/**
		* @ORM\Column(type="string", nullable=false)
		*/
		protected $type;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		protected $skype;
                /**
		* @ORM\Column(type="string", nullable=false)
		*/
		protected $status = 0;

		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		 protected $email;

		 /**
		* @ORM\Column(type="string", nullable=true)
		*/
		 protected $summoner;
		 /**
		* @ORM\Column(type="string", nullable=true)
		*/
		 protected $server;
		 /**
		* @ORM\Column(type="string", nullable=true)
		*/
		 protected $roles;
		 /**
		* @ORM\Column(type="string", nullable=true)
		*/
		 protected $laguage;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		 protected $age;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		 protected $persent;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		 protected $hours;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		 protected $coach;
		/**
		* @ORM\Column(type="text", nullable=true)
		*/
		 protected $message;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		protected $discord;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		protected $fullName;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		protected $nickname;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		protected $whereAreYouFrom;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		protected $specs;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		protected $csGoRank;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		protected $serviceType;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		protected $abilityToUseEnglish;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		protected $workOrStudy;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		protected $testBoost;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		protected $stableInternet;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		protected $boosterExperience;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		protected $accountScreenShot;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		protected $profile;
        /**
        * @ORM\Column(type="string", nullable=true)
        */
        protected $sr;
        /**
        * @ORM\Column(type="string", nullable=true)
        */
        protected $platform;
        /**
        * @ORM\Column(type="string", nullable=true)
        */
        protected $stream;
        /**
        * @ORM\Column(type="string", nullable=true)
        */
        protected $DuofromOwnAccount;
        /**
        * @ORM\Column(type="string", nullable=true)
        */
        protected $rank;
        /**
        * @ORM\Column(type="string", nullable=true)
        */
        protected $battleTag;
        /**
        * @ORM\Column(type="string", nullable=true)
        */
        protected $steamId;
        /**
        * @ORM\Column(type="string", nullable=true)
        */
        protected $guildName;
        /**
        * @ORM\Column(type="string", nullable=true)
        */
        protected $faction;
        /**
        * @ORM\Column(type="string", nullable=true)
        */
        protected $highestRankaccountScreenShot;
		/**
		 * @ORM\Column(type="datetime", nullable=true)
		 *
		 * @var \DateTime
		 */
		private $Created_at;

		/**
		*
		* @ORM\PrePersist
		* @ORM\PreUpdate
		*/
		public function updatedTimestamps(){
		
		   $this->setCreated_at(new \DateTime('now'));

		   if ($this->getCreated_at() == null) {
		       $this->setCreated_at(new \DateTime('now'));
		   }
		}
		
		public function setSkype($skype) {
		
		    $this->skype = $skype;
		
		    return $this;
		}
		
		public function getSkype() {
		
		    return $this->skype;
		}
		public function setId($id) {
		
		    $this->id = $id;
		
		    return $this;
		}
		
		public function getId() {
		
		    return $this->id;
		}
		public function setEmail($email) {
		
		    $this->email = $email;
		
		    return $this;
		}
		
		public function getEmail() {
		
		    return $this->email;
		}
		
		public function setSumonner($sumonner) {
		
		    $this->sumonner = $sumonner;
		
		    return $this;
		}
		
		public function getSumonner() {
		
		    return $this->sumonner;
		}
		public function setServer($server) {
		
		    $this->server = $server;
		
		    return $this;
		}
		
		public function getServer() {
		
		    return $this->server;
		}
		public function setRoles($roles) {
		
		    $this->roles = $roles;
		
		    return $this;
		}
		
		public function getRoles() {
		
		    return $this->roles;
		}

		public function setLaguage($laguage) {
		
		    $this->laguage = $laguage;
		
		    return $this;
		}
		
		public function getLaguage() {
		
		    return $this->laguage;
		}

		public function setAge($age) {
		
		    $this->age = $age;
		
		    return $this;
		}
		
		public function getAge() {
		
		    return $this->age;
		}

		public function setPersent($persent) {
		
		    $this->persent = $persent;
		
		    return $this;
		}
		
		public function getPersent() {
		
		    return $this->persent;
		}

		public function setHours($hours) {
		
		    $this->hours = $hours;
		
		    return $this;
		}
		
		public function getHours() {
		
		    return $this->hours;
		}

		public function setCoach($coach) {
		
		    $this->coach = $coach;
		
		    return $this;
		}
		
		public function getCoach() {
		
		    return $this->coach;
		}

		public function setMessage($message) {
		
		    $this->message = $message;
		
		    return $this;
		}
		
		public function getMessage() {
		
		    return $this->message;
		}

		public function setCreated_at($Created_at) {
		
		    $this->Created_at = $Created_at;
		
		    return $this;
		}
		
		public function getCreated_at() {
		
		    return $this->Created_at;
		}


    /**
     * Set type
     *
     * @param string $type
     *
     * @return Job
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Job
     */
    public function setCreatedAt($createdAt)
    {
        $this->Created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->Created_at;
    }

    /**
     * Set discord
     *
     * @param string $discord
     *
     * @return Job
     */
    public function setDiscord($discord)
    {
        $this->discord = $discord;

        return $this;
    }

    /**
     * Get discord
     *
     * @return string
     */
    public function getDiscord()
    {
        return $this->discord;
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     *
     * @return Job
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set nickname
     *
     * @param string $nickname
     *
     * @return Job
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * Get nickname
     *
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * Set from
     *
     * @param string $from
     *
     * @return Job
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get from
     *
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set specs
     *
     * @param string $specs
     *
     * @return Job
     */
    public function setSpecs($specs)
    {
        $this->specs = $specs;

        return $this;
    }

    /**
     * Get specs
     *
     * @return string
     */
    public function getSpecs()
    {
        return $this->specs;
    }

    /**
     * Set csGoRank
     *
     * @param string $csGoRank
     *
     * @return Job
     */
    public function setCsGoRank($csGoRank)
    {
        $this->csGoRank = $csGoRank;

        return $this;
    }

    /**
     * Get csGoRank
     *
     * @return string
     */
    public function getCsGoRank()
    {
        return $this->csGoRank;
    }

    /**
     * Set serviceType
     *
     * @param string $serviceType
     *
     * @return Job
     */
    public function setServiceType($serviceType)
    {
        $this->serviceType = $serviceType;

        return $this;
    }

    /**
     * Get serviceType
     *
     * @return string
     */
    public function getServiceType()
    {
        return $this->serviceType;
    }

    /**
     * Set abilityToUseEnglish
     *
     * @param string $abilityToUseEnglish
     *
     * @return Job
     */
    public function setAbilityToUseEnglish($abilityToUseEnglish)
    {
        $this->abilityToUseEnglish = $abilityToUseEnglish;

        return $this;
    }

    /**
     * Get abilityToUseEnglish
     *
     * @return string
     */
    public function getAbilityToUseEnglish()
    {
        return $this->abilityToUseEnglish;
    }

    /**
     * Set workOrStudy
     *
     * @param string $workOrStudy
     *
     * @return Job
     */
    public function setWorkOrStudy($workOrStudy)
    {
        $this->workOrStudy = $workOrStudy;

        return $this;
    }

    /**
     * Get workOrStudy
     *
     * @return string
     */
    public function getWorkOrStudy()
    {
        return $this->workOrStudy;
    }

    /**
     * Set testBoost
     *
     * @param string $testBoost
     *
     * @return Job
     */
    public function setTestBoost($testBoost)
    {
        $this->testBoost = $testBoost;

        return $this;
    }

    /**
     * Get testBoost
     *
     * @return string
     */
    public function getTestBoost()
    {
        return $this->testBoost;
    }

    /**
     * Set stableInternet
     *
     * @param string $stableInternet
     *
     * @return Job
     */
    public function setStableInternet($stableInternet)
    {
        $this->stableInternet = $stableInternet;

        return $this;
    }

    /**
     * Get stableInternet
     *
     * @return string
     */
    public function getStableInternet()
    {
        return $this->stableInternet;
    }

    /**
     * Set boosterExperience
     *
     * @param string $boosterExperience
     *
     * @return Job
     */
    public function setBoosterExperience($boosterExperience)
    {
        $this->boosterExperience = $boosterExperience;

        return $this;
    }

    /**
     * Get boosterExperience
     *
     * @return string
     */
    public function getBoosterExperience()
    {
        return $this->boosterExperience;
    }

    /**
     * Set accountScreenShot
     *
     * @param string $accountScreenShot
     *
     * @return Job
     */
    public function setAccountScreenShot($accountScreenShot)
    {
        $this->accountScreenShot = $accountScreenShot;

        return $this;
    }

    /**
     * Get accountScreenShot
     *
     * @return string
     */
    public function getAccountScreenShot()
    {
        return $this->accountScreenShot;
    }

    /**
     * Set profile
     *
     * @param string $profile
     *
     * @return Job
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile
     *
     * @return string
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set whereAreYouFrom
     *
     * @param string $whereAreYouFrom
     *
     * @return Job
     */
    public function setWhereAreYouFrom($whereAreYouFrom)
    {
        $this->whereAreYouFrom = $whereAreYouFrom;

        return $this;
    }

    /**
     * Get whereAreYouFrom
     *
     * @return string
     */
    public function getWhereAreYouFrom()
    {
        return $this->whereAreYouFrom;
    }

    /**
     * Set sr
     *
     * @param string $sr
     *
     * @return Job
     */
    public function setSr($sr)
    {
        $this->sr = $sr;

        return $this;
    }

    /**
     * Get sr
     *
     * @return string
     */
    public function getSr()
    {
        return $this->sr;
    }

    /**
     * Set platform
     *
     * @param string $platform
     *
     * @return Job
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;

        return $this;
    }

    /**
     * Get platform
     *
     * @return string
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * Set stream
     *
     * @param string $stream
     *
     * @return Job
     */
    public function setStream($stream)
    {
        $this->stream = $stream;

        return $this;
    }

    /**
     * Get stream
     *
     * @return string
     */
    public function getStream()
    {
        return $this->stream;
    }

    /**
     * Set overwatchDuofromOwnAccount
     *
     * @param string $overwatchDuofromOwnAccount
     *
     * @return Job
     */
    public function setOverwatchDuofromOwnAccount($overwatchDuofromOwnAccount)
    {
        $this->overwatchDuofromOwnAccount = $overwatchDuofromOwnAccount;

        return $this;
    }

    /**
     * Get overwatchDuofromOwnAccount
     *
     * @return string
     */
    public function getOverwatchDuofromOwnAccount()
    {
        return $this->overwatchDuofromOwnAccount;
    }

    /**
     * Set rank
     *
     * @param string $rank
     *
     * @return Job
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return string
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Set battleTag
     *
     * @param string $battleTag
     *
     * @return Job
     */
    public function setBattleTag($battleTag)
    {
        $this->battleTag = $battleTag;

        return $this;
    }

    /**
     * Get battleTag
     *
     * @return string
     */
    public function getBattleTag()
    {
        return $this->battleTag;
    }

    /**
     * Set highestRankaccountScreenShot
     *
     * @param string $highestRankaccountScreenShot
     *
     * @return Job
     */
    public function setHighestRankaccountScreenShot($highestRankaccountScreenShot)
    {
        $this->highestRankaccountScreenShot = $highestRankaccountScreenShot;

        return $this;
    }

    /**
     * Get highestRankaccountScreenShot
     *
     * @return string
     */
    public function getHighestRankaccountScreenShot()
    {
        return $this->highestRankaccountScreenShot;
    }

    /**
     * Set duofromOwnAccount
     *
     * @param string $duofromOwnAccount
     *
     * @return Job
     */
    public function setDuofromOwnAccount($duofromOwnAccount)
    {
        $this->DuofromOwnAccount = $duofromOwnAccount;

        return $this;
    }

    /**
     * Get duofromOwnAccount
     *
     * @return string
     */
    public function getDuofromOwnAccount()
    {
        return $this->DuofromOwnAccount;
    }

    /**
     * Set steamId
     *
     * @param string $steamId
     *
     * @return Job
     */
    public function setSteamId($steamId)
    {
        $this->steamId = $steamId;

        return $this;
    }

    /**
     * Get steamId
     *
     * @return string
     */
    public function getSteamId()
    {
        return $this->steamId;
    }

    /**
     * Set guildName
     *
     * @param string $guildName
     *
     * @return Job
     */
    public function setGuildName($guildName)
    {
        $this->guildName = $guildName;

        return $this;
    }

    /**
     * Get guildName
     *
     * @return string
     */
    public function getGuildName()
    {
        return $this->guildName;
    }

    /**
     * Set faction
     *
     * @param string $faction
     *
     * @return Job
     */
    public function setFaction($faction)
    {
        $this->faction = $faction;

        return $this;
    }

    /**
     * Get faction
     *
     * @return string
     */
    public function getFaction()
    {
        return $this->faction;
    }

    /**
     * Set summoner
     *
     * @param string $summoner
     *
     * @return Job
     */
    public function setSummoner($summoner)
    {
        $this->summoner = $summoner;

        return $this;
    }

    /**
     * Get summoner
     *
     * @return string
     */
    public function getSummoner()
    {
        return $this->summoner;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Job
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
}
