<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PaymentRepository")
 * @ORM\Table(name="Paypal")
@ORM\HasLifecycleCallbacks
*/
class Paypal
{
		/**
		* @ORM\Id;
		* @ORM\Column(type="integer")
		* @ORM\GeneratedValue(strategy="AUTO")
		*/
		protected $id;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		protected $type;
		/**
		* @ORM\Column(type="text", nullable=true)
		*/
		 protected $description;

		 /**
		* @ORM\Column(type="string", nullable=true)
		*/
		 protected $txn_id;
		 /**
		* @ORM\Column(type="string", nullable=true)
		*/
		 protected $parent_txn_id;
		 /**
		* @ORM\Column(type="string", nullable=true)
		*/
		 protected $payment_status;
		/**
		* @ORM\Column(type="text", nullable=true)
		*/
		 protected $details;
		/**
		 * @ORM\Column(type="datetime")
		 *
		 * @var \DateTime
		 */
		private $Created_at;

		/**
		*
		* @ORM\PrePersist
		* @ORM\PreUpdate
		*/
		public function updatedTimestamps(){
		
		   $this->setCreatedAt(new \DateTime('now'));

		   if ($this->getCreatedAt() == null) {
		       $this->setCreatedAt(new \DateTime('now'));
		   }
		}
		

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return IPN_PDT
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return IPN_PDT
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set txnId
     *
     * @param string $txnId
     *
     * @return IPN_PDT
     */
    public function setTxnId($txnId)
    {
        $this->txn_id = $txnId;

        return $this;
    }

    /**
     * Get txnId
     *
     * @return string
     */
    public function getTxnId()
    {
        return $this->txn_id;
    }

    /**
     * Set parentTxnId
     *
     * @param string $parentTxnId
     *
     * @return IPN_PDT
     */
    public function setParentTxnId($parentTxnId)
    {
        $this->parent_txn_id = $parentTxnId;

        return $this;
    }

    /**
     * Get parentTxnId
     *
     * @return string
     */
    public function getParentTxnId()
    {
        return $this->parent_txn_id;
    }

    /**
     * Set paymentStatus
     *
     * @param string $paymentStatus
     *
     * @return IPN_PDT
     */
    public function setPaymentStatus($paymentStatus)
    {
        $this->payment_status = $paymentStatus;

        return $this;
    }

    /**
     * Get paymentStatus
     *
     * @return string
     */
    public function getPaymentStatus()
    {
        return $this->payment_status;
    }

    /**
     * Set details
     *
     * @param string $details
     *
     * @return IPN_PDT
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Get details
     *
     * @return string
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return IPN_PDT
     */
    public function setCreatedAt($createdAt)
    {
        $this->Created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->Created_at;
    }
}
