<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\logRepository")
 * @ORM\Table(name="log")
@ORM\HasLifecycleCallbacks
*/
class Log
{

		/**
		* @ORM\Id;
		* @ORM\Column(type="integer")
		* @ORM\GeneratedValue(strategy="AUTO")
		*/
		protected $id;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		protected $userId;
		/**
		* @ORM\Column(type="string")
		*/
		 protected $action;

		 /**
		* @ORM\Column(type="string", nullable=true)
		*/
		 protected $ip;
		 /**
		* @ORM\Column(type="string", nullable=true)
		*/
		 protected $browser;
		 /**
		* @ORM\Column(type="string", nullable=true)
		*/
		 protected $referrer;
		/**
		* @ORM\Column(type="text", nullable=true)
		*/
		 protected $details;
		/**
		 * @ORM\Column(type="datetime")
		 *
		 * @var \DateTime
		 */
		private $Created_at;

		/**
		*
		* @ORM\PrePersist
		* @ORM\PreUpdate
		*/
		public function updatedTimestamps(){
		
		   $this->setCreated_at(new \DateTime('now'));

		   if ($this->getCreated_at() == null) {
		       $this->setCreated_at(new \DateTime('now'));
		   }
		}
		

		
		public function setUserId($userId) {
		
		    $this->userId = $userId;
		
		    return $this;
		}
		
		public function getUserId() {
		
		    return $this->userId;
		}
		
		public function setAction($action) {
		
		    $this->action = $action;
		
		    return $this;
		}
		
		public function getAction() {
		
		    return $this->action;
		}
		
		public function setReferrer($referrer) {
		
		    $this->referrer = $referrer;
		
		    return $this;
		}
		
		public function getReferrere() {
		
		    return $this->referrer;
		}
		public function setBrowser($browser) {
		
		    $this->browser = $browser;
		
		    return $this;
		}
		
		public function getBrowser() {
		
		    return $this->browser;
		}
		public function setIp($ip) {
		
		    $this->ip = $ip;
		
		    return $this;
		}
		
		public function getIp() {
		
		    return $this->ip;
		}

		public function setCreated_at($Created_at) {
		
		    $this->Created_at = $Created_at;
		
		    return $this;
		}
		
		public function getCreated_at() {
		
		    return $this->Created_at;
		}


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get referrer
     *
     * @return string
     */
    public function getReferrer()
    {
        return $this->referrer;
    }

    /**
     * Set details
     *
     * @param string $details
     *
     * @return Log
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Get details
     *
     * @return string
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Log
     */
    public function setCreatedAt($createdAt)
    {
        $this->Created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->Created_at;
    }
}
