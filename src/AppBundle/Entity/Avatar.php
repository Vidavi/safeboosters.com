<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ImageRepository")
 * @ORM\Table(name="avatar")
 */

class Avatar
{
    /**
 * @ORM\Id
 * @ORM\Column(type="integer")
 * @ORM\GeneratedValue(strategy="AUTO")
 */
private $id;


    /**

     * @ORM\Column(name="avatar", type="string", nullable=true)

     */
    private $avatar;
    /**
    * @ORM\Column(type="string")
    */
    protected $imagePath;
    /**
    * @ORM\Column(type="string")
    */
    protected $imageOriginalName;
    /**
    * @ORM\Column(type="string")
    */
    protected $userId;
     /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $createdAt;

   



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     *
     * @return Avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     *
     * @return Avatar
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * Set imageOriginalName
     *
     * @param string $imageOriginalName
     *
     * @return Avatar
     */
    public function setImageOriginalName($imageOriginalName)
    {
        $this->imageOriginalName = $imageOriginalName;

        return $this;
    }

    /**
     * Get imageOriginalName
     *
     * @return string
     */
    public function getImageOriginalName()
    {
        return $this->imageOriginalName;
    }

    /**
     * Set userId
     *
     * @param string $userId
     *
     * @return Avatar
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set createdAt
     *
     * @param string $createdAt
     *
     * @return Avatar
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
