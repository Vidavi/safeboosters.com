<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\forumRepository")
 * @ORM\Table(name="Post")
@ORM\HasLifecycleCallbacks
*/
class Post
{
		/**
		* @ORM\Id;
		* @ORM\Column(type="integer")
		* @ORM\GeneratedValue(strategy="AUTO")
		*/
		protected $id;
		/**
		* @ORM\Column(type="text")
   
/**
     * @Assert\Length(
     *      min = 40,
     *      max = 1000,
     *      minMessage = "Your post must be at least {{ limit }} characters long",
     *      maxMessage = "Your post cannot be longer than {{ limit }} characters"
     * )
     *@Assert\NotBlank()

		*/

		protected $post_content;
                /**
		* @ORM\Column(type="boolean")
		*/
		protected $status = false;
        /**
        * @ORM\Column(type="string")
        */
        protected $user_name;
		/**
		* @ORM\Column(type="integer")
		*/
		protected $post_topic;
		/**
        * @ORM\Column(type="integer")
        */
        protected $post_by;
        /**
        * @ORM\Column(type="datetime", nullable=true)
        *
        * @var \DateTime
        */
        protected $last_edit_time;
		/**
		* @ORM\Column(type="datetime")
		*
		* @var \DateTime
		*/
		protected $Created_at;

		/**
		*
		* @ORM\PrePersist
		* @ORM\PreUpdate
		*/
		// public function updatedTimestamps(){
		// 
		//    $this->setCreatedAt(new \DateTime('now'));
// 
		//    if ($this->getCreatedAt() == null) {
		//        $this->setCreatedAt(new \DateTime('now'));
		//    }
		// }
		
	

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set postContent
     *
     * @param string $postContent
     *
     * @return Post
     */
    public function setPostContent($postContent)
    {
        $this->post_content = $postContent;

        return $this;
    }

    /**
     * Get postContent
     *
     * @return string
     */
    public function getPostContent()
    {
        return $this->post_content;
    }

    /**
     * Set checkk
     *
     * @param boolean $checkk
     *
     * @return Post
     */
    public function setCheckk($checkk)
    {
        $this->checkk = $checkk;

        return $this;
    }

    /**
     * Get checkk
     *
     * @return boolean
     */
    public function getCheckk()
    {
        return $this->checkk;
    }

    /**
     * Set userName
     *
     * @param string $userName
     *
     * @return Post
     */
    public function setUserName($userName)
    {
        $this->user_name = $userName;

        return $this;
    }

    /**
     * Get userName
     *
     * @return string
     */
    public function getUserName()
    {
        return $this->user_name;
    }

    /**
     * Set postTopic
     *
     * @param integer $postTopic
     *
     * @return Post
     */
    public function setPostTopic($postTopic)
    {
        $this->post_topic = $postTopic;

        return $this;
    }

    /**
     * Get postTopic
     *
     * @return integer
     */
    public function getPostTopic()
    {
        return $this->post_topic;
    }

    /**
     * Set postBy
     *
     * @param integer $postBy
     *
     * @return Post
     */
    public function setPostBy($postBy)
    {
        $this->post_by = $postBy;

        return $this;
    }

    /**
     * Get postBy
     *
     * @return integer
     */
    public function getPostBy()
    {
        return $this->post_by;
    }

    /**
     * Set lastEditTime
     *
     * @param \DateTime $lastEditTime
     *
     * @return Post
     */
    public function setLastEditTime($lastEditTime)
    {
        $this->last_edit_time = $lastEditTime;

        return $this;
    }

    /**
     * Get lastEditTime
     *
     * @return \DateTime
     */
    public function getLastEditTime()
    {
        return $this->last_edit_time;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Post
     */
    public function setCreatedAt($createdAt)
    {
        $this->Created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->Created_at;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Post
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }
}
