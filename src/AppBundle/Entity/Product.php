<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
 * @ORM\Entity
 * @Vich\Uploadable
 */
class Product
{

/**
 * @ORM\Id
 * @ORM\Column(type="integer")
 * @ORM\GeneratedValue(strategy="AUTO")
 */
private $id;

/**
 * @ORM\Column(type="integer", nullable=false)
 */
private $userId;
/**
 *
 * @Vich\UploadableField(mapping="product_image", fileNameProperty="imageName")
 *
 * @var File
 */
private $imageFile;

/**
 * @ORM\Column(type="string", length=255)
 *
 * @var string
 */
private $imageName;

/**
 * @ORM\Column(type="datetime")
 *
 * @var \DateTime
 */
private $updatedAt;

/**
 * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
 *
 * @return Actualite
 */

public function setImageFile(File $image = null)
{
    $this->imageFile = $image;

    if ($image) {
        // It is required that at least one field changes if you are using doctrine
        // otherwise the event listeners won't be called and the file is lost
        $this->updatedAt = new \DateTime('now');
    }

    return $this;
}

/**
 * @return File
 */
public function getImageFile()
{
    return $this->imageFile;
}

/**
 * @param string $imageName
 *
 * @return Actualite
 */
public function setImageName($imageName)
{
    $this->imageName = $imageName;

    return $this;
}

/**
 * @return string
 */
public function getImageName()
{
    return $this->imageName;
}

public function setUserId($userId)
{
    $this->userId = $userId;

    return $this;
}

/**
 * @return string
 */
public function getUserId()
{
    return $this->userId;
}

}