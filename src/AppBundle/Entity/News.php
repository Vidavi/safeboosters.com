<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\newsRepository")
 * @ORM\Table(name="News")
@ORM\HasLifecycleCallbacks
*/
class News
{

	/**
	* @ORM\Id;
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;
	/**
	* @ORM\Column(type="string", nullable=false)
	*/
	protected $message_name;
	/**
	* @ORM\Column(type="integer", nullable=true)
	*/
	 protected $user_id;
    /**
    * @ORM\Column(type="integer", nullable=true)
    */
     protected $message_type;
	 /**
	* @ORM\Column(type="integer", nullable=true)
	*/
	 protected $topic_id;
	 /**
	* @ORM\Column(type="integer", nullable=true)
	*/
	 protected $job_id;
         /**
	* @ORM\Column(type="integer", nullable=true)
	*/
	 protected $postId;
	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 *
	 * @var \DateTime
	 */
	private $Created_at;
		
	
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set messageName
     *
     * @param string $messageName
     *
     * @return News
     */
    public function setMessageName($messageName)
    {
        $this->message_name = $messageName;

        return $this;
    }

    /**
     * Get messageName
     *
     * @return string
     */
    public function getMessageName()
    {
        return $this->message_name;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return News
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set topicId
     *
     * @param integer $topicId
     *
     * @return News
     */
    public function setTopicId($topicId)
    {
        $this->topic_id = $topicId;

        return $this;
    }

    /**
     * Get topicId
     *
     * @return integer
     */
    public function getTopicId()
    {
        return $this->topic_id;
    }

    /**
     * Set jobId
     *
     * @param integer $jobId
     *
     * @return News
     */
    public function setJobId($jobId)
    {
        $this->job_id = $jobId;

        return $this;
    }

    /**
     * Get jobId
     *
     * @return integer
     */
    public function getJobId()
    {
        return $this->job_id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return News
     */
    public function setCreatedAt($createdAt)
    {
        $this->Created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->Created_at;
    }

    /**
     * Set messageType
     *
     * @param integer $messageType
     *
     * @return News
     */
    public function setMessageType($messageType)
    {
        $this->message_type = $messageType;

        return $this;
    }

    /**
     * Get messageType
     *
     * @return integer
     */
    public function getMessageType()
    {
        return $this->message_type;
    }

    /**
     * Set postId
     *
     * @param integer $postId
     *
     * @return News
     */
    public function setPostId($postId)
    {
        $this->postId = $postId;

        return $this;
    }

    /**
     * Get postId
     *
     * @return integer
     */
    public function getPostId()
    {
        return $this->postId;
    }
}
