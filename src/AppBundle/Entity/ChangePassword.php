<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;

class ChangePassword
{

/**
 * @Assert\Type(type="AppBundle\Entity\User")
 * @Assert\Valid()
 */
protected $user;

/**
 * @SecurityAssert\UserPassword(
 *     message = "Wrong value for your current password"
 * )
 */
 protected $password;

/**
 * @Assert\Length(
 *     min = 5,
 *     minMessage = "Password should by at least 6 chars long"
 * )
 */
 protected $plainPassword;


public function setUser(User $user)
{
    $this->user = $user;
}

public function getUser()
{
    return $this->user;
}

public function setPassword($password) {

    $this->password = $password;

    return $this;
}

public function getPassword() {

    return $this->password;
}

public function setPlainPassword($plainPassword) {

    $this->plainPassword = $plainPassword;

    return $this;
}

public function getPlainPassword() {

    return $this->plainPassword;
}
}