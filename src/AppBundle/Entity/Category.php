<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
* @ORM\Entity
* @UniqueEntity(fields="cat_name", message="This category already exist!")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\forumRepository")
@ORM\HasLifecycleCallbacks
*/
class Category
{
		/**
		* @ORM\Id;
		* @ORM\Column(type="integer")
		* @ORM\GeneratedValue(strategy="AUTO")
		*/
		protected $id;
		/**
		* @ORM\Column(type="string")
		*/
		 protected $cat_name;

		 /**
		* @ORM\Column(type="integer", options={"default" = 0})
		*/
		protected $cat_topic_sum;
		/**
		* @ORM\Column(type="integer", options={"default" = 0})
		*/
		protected $cat_post_sum;
		/**
		* @ORM\Column(type="string")
		*/
		protected $cat_description;
		/**
		* @ORM\Column(type="integer", nullable=true)
		*/
		 protected $last_topic_by_user_id;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		 protected $last_topic_by_user_name;
		/**
		* @ORM\Column(type="datetime", nullable=true)
		*/
		 protected $last_topic_date;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		 protected $path;

		public function increaseTopicCounter()
    	{
    	    $this->cat_topic_sum++;
    	}
    	public function decreaseTopicCounter()
    	{
    	    $this->cat_topic_sum--;
    	}
    	public function increasePostCounter()
    	{
    	    $this->cat_post_sum++;
    	}
    	public function decreasePostCounter()
    	{
    	    $this->cat_post_sum--;
    	}

		 public function setId($id) {
		
		    $this->id = $id;
		
		    return $this;
		}
		
		public function getId() {
		
		    return $this->id;
		}
		
		public function setCat_name($cat_name) {
		
		    $this->cat_name = $cat_name;
		
		    return $this;
		}
		
		public function getCat_name() {
		
		    return $this->cat_name;
		}
		
		public function setCat_description($cat_description) {
		
		    $this->cat_description = $cat_description;
		
		    return $this;
		}
		
		public function getCat_description() {
		
		    return $this->cat_description;
		}
		public function setCat_topic_sum($cat_topic_sum) {
		
		    $this->cat_topic_sum = $cat_topic_sum;
		
		    return $this;
		}
		
		public function getCat_topic_sum() {
		
		    return $this->cat_topic_sum;
		}
		public function setCat_post_sum($cat_post_sum) {
		
		    $this->cat_post_sum = $cat_post_sum;
		
		    return $this;
		}
		
		public function getCat_post_sum() {
		
		    return $this->cat_post_sum;
		}
		public function setPath($path) {
		
		    $this->path = $path;
		
		    return $this;
		}
		
		public function getPath() {
		
		    return $this->path;
		}
		public function setLast_topic_by_user_id($last_topic_by_user_id) {
		
		    $this->last_topic_by_user_id = $last_topic_by_user_id;
		
		    return $this;
		}
		
		public function getLast_topic_by_user_id() {
		
		    return $this->last_topic_by_user_id;
		}
		public function setLast_topic_by_user_name($last_topic_by_user_name) {
		
		    $this->last_topic_by_user_name = $last_topic_by_user_name;
		
		    return $this;
		}
		
		public function getLast_topic_by_user_name() {
		
		    return $this->last_topic_by_user_name;
		}
		public function setLast_topic_date($last_topic_date) {
		
		    $this->last_topic_date = $last_topic_date;
		
		    return $this;
		}
		
		public function getLast_topic_date() {
		
		    return $this->last_topic_date;
		}

}