<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\forumRepository")
 * @ORM\Table(name="topic")
@ORM\HasLifecycleCallbacks
*/
class Topic
{
		/**
		* @ORM\Id;
		* @ORM\Column(type="integer")
		* @ORM\GeneratedValue(strategy="AUTO")
		*/
		protected $id;
                /**

                * @ORM\Column(name="brochure", type="array", nullable=true)
           
                */
                private $brochure;
                 /**
		* @ORM\Column(type="string")
		*/
		protected $topic_name;
                /**
		* @ORM\Column(type="boolean")
		*/
		protected $status = false;
		/**
		* @ORM\Column(type="string")
		*/
		protected $scamer_name;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		protected $scammer_IGN;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		protected $scammer_email;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		protected $scammer_skype;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		protected $scammer_contactApp;
		/**
		* @ORM\Column(type="string")
		*/
		protected $cat_name;
		/**
		* @ORM\Column(type="integer")
		*/
		protected $topic_cat;
		/**
		* @ORM\Column(type="integer")
		*/
		protected $topic_by; 
		/**
		/**
		* @ORM\Column(type="string")
		*/
		protected $topic_by_name; 
		/**
		* @ORM\Column(type="integer")
		*/
		protected $cat_voices_sum = 0;
		/**
		* @ORM\Column(type="integer")
		*/
		protected $cat_post_sum = 0;

		/**
		* @ORM\Column(type="integer", nullable=true)
		*/
		protected $last_post_by_user_id;
		/**
		* @ORM\Column(type="string", nullable=true)
		*/
		 protected $last_post_by_user_name;
		/**
		* @ORM\Column(type="datetime", nullable=true)
		*/
		 protected $last_post_date;
		/**
		* @ORM\Column(type="datetime", nullable=true)
		*
		* @var \DateTime
		*/
		 protected $last_edit_time;

		/**
		* @ORM\Column(type="datetime")
		*
		* @var \DateTime
		*/
		private $Created_at;
		/**
		* @ORM\Column(type="text")
		*/
		protected $story;

        public function increaseVoicesCounter()
        {
            $this->cat_voices_sum++;
        }
        public function decreaseVoicesCounter()
        {
            $this->cat_voices_sum--;
        }
        public function increasePostCounter()
        {
            $this->cat_post_sum++;
        }
        public function decreasePostCounter()
        {
            $this->cat_post_sum--;
        }

		/**
		*
		* @ORM\PrePersist
		* @ORM\PreUpdate
		*/
		public function updatedTimestamps(){
		
		   $this->setCreatedat(new \DateTime('now'));

		   if ($this->getCreatedAt() == null) {
		       $this->setCreatedAt(new \DateTime('now'));
		   }
		}
	
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set topicName
     *
     * @param string $topicName
     *
     * @return Topic
     */
    public function setTopicName($topicName)
    {
        $this->topic_name = $topicName;

        return $this;
    }

    /**
     * Get topicName
     *
     * @return string
     */
    public function getTopicName()
    {
        return $this->topic_name;
    }

    /**
     * Set scamerName
     *
     * @param string $scamerName
     *
     * @return Topic
     */
    public function setScamerName($scamerName)
    {
        $this->scamer_name = $scamerName;

        return $this;
    }

    /**
     * Get scamerName
     *
     * @return string
     */
    public function getScamerName()
    {
        return $this->scamer_name;
    }

    /**
     * Set scammerIGN
     *
     * @param string $scammerIGN
     *
     * @return Topic
     */
    public function setScammerIGN($scammerIGN)
    {
        $this->scammer_IGN = $scammerIGN;

        return $this;
    }

    /**
     * Get scammerIGN
     *
     * @return string
     */
    public function getScammerIGN()
    {
        return $this->scammer_IGN;
    }

    /**
     * Set scammerEmail
     *
     * @param string $scammerEmail
     *
     * @return Topic
     */
    public function setScammerEmail($scammerEmail)
    {
        $this->scammer_email = $scammerEmail;

        return $this;
    }

    /**
     * Get scammerEmail
     *
     * @return string
     */
    public function getScammerEmail()
    {
        return $this->scammer_email;
    }

    /**
     * Set scammerSkype
     *
     * @param string $scammerSkype
     *
     * @return Topic
     */
    public function setScammerSkype($scammerSkype)
    {
        $this->scammer_skype = $scammerSkype;

        return $this;
    }

    /**
     * Get scammerSkype
     *
     * @return string
     */
    public function getScammerSkype()
    {
        return $this->scammer_skype;
    }

    /**
     * Set scammerContactApp
     *
     * @param string $scammerContactApp
     *
     * @return Topic
     */
    public function setScammerContactApp($scammerContactApp)
    {
        $this->scammer_contactApp = $scammerContactApp;

        return $this;
    }

    /**
     * Get scammerContactApp
     *
     * @return string
     */
    public function getScammerContactApp()
    {
        return $this->scammer_contactApp;
    }

    /**
     * Set catName
     *
     * @param string $catName
     *
     * @return Topic
     */
    public function setCatName($catName)
    {
        $this->cat_name = $catName;

        return $this;
    }

    /**
     * Get catName
     *
     * @return string
     */
    public function getCatName()
    {
        return $this->cat_name;
    }

    /**
     * Set topicCat
     *
     * @param integer $topicCat
     *
     * @return Topic
     */
    public function setTopicCat($topicCat)
    {
        $this->topic_cat = $topicCat;

        return $this;
    }

    /**
     * Get topicCat
     *
     * @return integer
     */
    public function getTopicCat()
    {
        return $this->topic_cat;
    }

    /**
     * Set topicBy
     *
     * @param integer $topicBy
     *
     * @return Topic
     */
    public function setTopicBy($topicBy)
    {
        $this->topic_by = $topicBy;

        return $this;
    }

    /**
     * Get topicBy
     *
     * @return integer
     */
    public function getTopicBy()
    {
        return $this->topic_by;
    }

    /**
     * Set topicByName
     *
     * @param string $topicByName
     *
     * @return Topic
     */
    public function setTopicByName($topicByName)
    {
        $this->topic_by_name = $topicByName;

        return $this;
    }

    /**
     * Get topicByName
     *
     * @return string
     */
    public function getTopicByName()
    {
        return $this->topic_by_name;
    }

    /**
     * Set catVoicesSum
     *
     * @param integer $catVoicesSum
     *
     * @return Topic
     */
    public function setCatVoicesSum($catVoicesSum)
    {
        $this->cat_voices_sum = $catVoicesSum;

        return $this;
    }

    /**
     * Get catVoicesSum
     *
     * @return integer
     */
    public function getCatVoicesSum()
    {
        return $this->cat_voices_sum;
    }

    /**
     * Set catPostSum
     *
     * @param integer $catPostSum
     *
     * @return Topic
     */
    public function setCatPostSum($catPostSum)
    {
        $this->cat_post_sum = $catPostSum;

        return $this;
    }

    /**
     * Get catPostSum
     *
     * @return integer
     */
    public function getCatPostSum()
    {
        return $this->cat_post_sum;
    }

    /**
     * Set lastPostByUserId
     *
     * @param integer $lastPostByUserId
     *
     * @return Topic
     */
    public function setLastPostByUserId($lastPostByUserId)
    {
        $this->last_post_by_user_id = $lastPostByUserId;

        return $this;
    }

    /**
     * Get lastPostByUserId
     *
     * @return integer
     */
    public function getLastPostByUserId()
    {
        return $this->last_post_by_user_id;
    }

    /**
     * Set lastPostByUserName
     *
     * @param string $lastPostByUserName
     *
     * @return Topic
     */
    public function setLastPostByUserName($lastPostByUserName)
    {
        $this->last_post_by_user_name = $lastPostByUserName;

        return $this;
    }

    /**
     * Get lastPostByUserName
     *
     * @return string
     */
    public function getLastPostByUserName()
    {
        return $this->last_post_by_user_name;
    }

    /**
     * Set lastPostDate
     *
     * @param \DateTime $lastPostDate
     *
     * @return Topic
     */
    public function setLastPostDate($lastPostDate)
    {
        $this->last_post_date = $lastPostDate;

        return $this;
    }

    /**
     * Get lastPostDate
     *
     * @return \DateTime
     */
    public function getLastPostDate()
    {
        return $this->last_post_date;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Topic
     */
    public function setCreatedAt($createdAt)
    {
        $this->Created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->Created_at;
    }

    /**
     * Set story
     *
     * @param string $story
     *
     * @return Topic
     */
    public function setStory($story)
    {
        $this->story = $story;

        return $this;
    }

    /**
     * Get story
     *
     * @return string
     */
    public function getStory()
    {
        return $this->story;
    }

    /**
     * Set lastEditTime
     *
     * @param \DateTime $lastEditTime
     *
     * @return Topic
     */
    public function setLastEditTime($lastEditTime)
    {
        $this->last_edit_time = $lastEditTime;

        return $this;
    }

    /**
     * Get lastEditTime
     *
     * @return \DateTime
     */
    public function getLastEditTime()
    {
        return $this->last_edit_time;
    }

    /**
     * Set check
     *
     * @param boolean $check
     *
     * @return Topic
     */
    public function setCheck($check)
    {
        $this->check = $check;

        return $this;
    }

    /**
     * Get check
     *
     * @return boolean
     */
    public function getCheck()
    {
        return $this->check;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Topic
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set brochure
     *
     * @param array $brochure
     *
     * @return Topic
     */
    public function setBrochure($brochure)
    {
        $this->brochure = $brochure;

        return $this;
    }

    /**
     * Get brochure
     *
     * @return array
     */
    public function getBrochure()
    {
        return $this->brochure;
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     *
     * @return Topic
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }
}
