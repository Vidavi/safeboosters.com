<?php
namespace AppBundle\DirectoryNamer;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;
use Vich\UploaderBundle\Mapping\PropertyMapping;
/**
 * Namer class.
 */
class DirectoryNamer implements DirectoryNamerInterface
{
public function directoryName($obj, PropertyMapping $mapping) {

		$dir = 'images';
        $username= $obj->getUserId();
if ($obj instanceof \AppBundle\Entity\Product){
    $dir = 'avatar';
}
        
        return $dir = $dir.'/'.$username;

    }
}