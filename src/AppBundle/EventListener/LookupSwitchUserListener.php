<?php
namespace AppBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Oneup\UploaderBundle\Event\PostPersistEvent;
use Symfony\Component\HttpFoundation\Request;
 use Doctrine\ORM\EntityManager;
 
class LookupSwitchUserListener implements EventSubscriberInterface
{
    private $Container;
    private $em;
    
   public function __construct(ContainerInterface $Container, EntityManager $em)
    {
        $this->Container = $Container;
        $this->em = $em;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => ['lookup', 12] // before the firewall
        ];
    }

    public function lookup(GetResponseEvent $event)
    {
        $request = $event->getRequest();

         if ($request->query->get('_morning_star_switch_user')) {
         
            return; // do nothing if already a _switch_user param present
        }

        if (!$request->query->get('_morning_star_switch_user_by_id')) {
           
            return; // do nothing if no _switch_user_by_id param
        }
        
        $ID = $request->query->get('_morning_star_switch_user_by_id');
        $repository = $this->em->getRepository('AppBundle:User')->getUSerById($ID);       
     
        $request->query->remove('_morning_star_switch_user_by_id');
        if($ID != '_exit'){
        $request->attributes->set('_morning_star_switch_user', $repository->getEmail());
        }
             
    }
}