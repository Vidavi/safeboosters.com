<?php
namespace AppBundle\EventListener;

 use AppBundle\Controller\InitController; 
 use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException; 
 use Symfony\Component\HttpKernel\Event\FilterControllerEvent; 
 use Symfony\Component\HttpFoundation\Session\Session; 
 use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
 use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
 use Twig_Environment;
 use Doctrine\ORM\EntityManager;
 use Symfony\Component\DependencyInjection\ContainerInterface;
 use Symfony\Component\HttpFoundation\Request;
 use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;


 class InitListener {

    private $authorizationChecker;
    private $tokenStorage;
    private $user;
    private $Session;
    private $Twig_Environment;
    private $ContainerInterface;
    private $em;
    public function __construct(AuthorizationCheckerInterface $authorizationChecker, TokenStorage $tokenStorage, Session $Session, Twig_Environment $Twig_Environment, EntityManager $em, ContainerInterface $ContainerInterface)
    {

        $this->authorizationChecker = $authorizationChecker;
        $this->tokenStorage = $tokenStorage;
        $this->Session = $Session;
        $this->Twig_Environment = $Twig_Environment;
        $this->ContainerInterface = $ContainerInterface;  
        $this->em = $em;
        $this->user = ($this->tokenStorage->getToken() != NULL) ? $this->tokenStorage->getToken()->getUser() : NULL;
                       

    }

     public function onKernelController(FilterControllerEvent $event)
     {


        $controller = $event->getController(); 
        $request = $event->getRequest();

         if (!is_array($controller)) {
             return;
         }

        if($this->tokenStorage->getToken() === null)
                return false;

        if ($this->authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY') && is_object($this->user)) {
         
             //   if( !$this->Session->has('user/avatar')){
                   //     $userId = $this->Session->get('user/id');
                        $userId = $this->user->getId();
                        // $repository = $this->em->getRepository('AppBundle:Product');
                       // $product = $repository->findOneBy(array('userId' => $userId));
            
                        //$helper = $this->ContainerInterface->get('vich_uploader.templating.helper.uploader_helper');
                         $checkimage = $this->em->getRepository('AppBundle:Avatar')->findOneBy(array('userId' => $userId));
                        if($checkimage === NULL){
                                $paths="/image/web/manager.png";
                        }else{
                                //$paths = $helper->asset($product, 'imageFile');
                            $paths = $checkimage->getImagePath().$checkimage->getAvatar().'';
                        }
                    
                        $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
                        $avatarPath = $baseurl . $paths;
                        
                      //  $this->Session->set('user/avatar', $avatarPath);
            //    }
            
                //$avatar = $this->Session->get('user/avatar');
                $this->Twig_Environment->addGlobal('avatar', $avatarPath);

                // USER ROLES (SET MEMBER)
                $user_role = $this->user->getRole();
                $user_sub_date = $this->user->getSubEndDate();
                $current_time = new \DateTime('now');
                $user = $this->user;
                if($user_role === "ROLE_USER" && $current_time < $user_sub_date){
                    $user->setRole('ROLE_MEMBER');
                    $this->em->persist($user);
                    $this->em->flush();
                    $oldToken = $this->tokenStorage->getToken();
                    $token = new UsernamePasswordToken(
                        $user, 
                        null,
                        $oldToken->getProviderKey(),
                        $user->getRoles());
                    $this->tokenStorage->setToken($token);
                }elseif($user_role === "ROLE_MEMBER" && $current_time > $user_sub_date){
                  $user->setRole('ROLE_USER');
                    $this->em->persist($user);
                    $this->em->flush();
                    $oldToken = $this->tokenStorage->getToken();
                    $token = new UsernamePasswordToken(
                        $user, 
                        null,
                        $oldToken->getProviderKey(),
                        $user->getRoles());
                    $this->tokenStorage->setToken($token);
                }

        }else{
    
                $this->Twig_Environment->addGlobal('user', null);
                $this->Twig_Environment->addGlobal('avatar', 'image/web/manager.png');
        }

         if ($controller[0] instanceof InitController) {

         }
        
      

     }

      public function user()
     {

     }

  }