<?php
namespace AppBundle\service;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Filesystem;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Image;
class FileUploader
{
   
    private $targetDir;
    private $avatarDir;
    private $em;
    private $defaultStoreDir;
    
    public function __construct($targetDir, $avatarDir, EntityManager $em)
    {
        $this->targetDir = $targetDir;
        $this->avatarDir = $avatarDir;
        $this->em = $em;
        $this->defaultStoreDir =  '/'. date('y/m').'/';
    }

    public function upload($files, $id)
    {
        foreach ($files as &$file)
        {

            $imageObject = new Image();
            
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            $file->move($this->getTargetDir() . $this->defaultStoreDir , $fileName);

            $imageObject->setFileId($id);
            $imageObject->setImagePath($this->defaultStoreDir);
            $imageObject->setBrochure($fileName);
            $imageObject->setImageOriginalName($file->getClientOriginalName());
            $now = new \DateTime('now');
            $date = $now->format('Y-m-d H:i:s');
            $imageObject->setCreatedAt($date);
            $this->em->persist($imageObject);    
        }
        
        $this->em->flush();

    }
    
    public function uploadAvatar($imagen, $id)
    {
      
       
        
            $imageObject = $imagen;
           $ss = $imageObject->getAvatar();
          //  die();
            $fileName = md5(uniqid()).'.'.$ss[0]->guessExtension();
          
            $ss[0]->move($this->getAvatarDir() . $this->defaultStoreDir , $fileName);
           // var_dump($imageObject->getAvatar());
           // die();
            $imageObject->setUserId($id);
            $imageObject->setImagePath('/uploads/avatar'.$this->defaultStoreDir);
           // var_dump($imageObject->getAvatar());
            //die();
            $imageObject->setImageOriginalName($ss[0]->getClientOriginalName());
             $imageObject->setAvatar($fileName);
            $now = new \DateTime('now');
            $date = $now->format('Y-m-d H:i:s');
            $imageObject->setCreatedAt($date);
            $this->em->persist($imageObject);    
        
        
        $this->em->flush();
return $imageObject;
    }
     public function remove($FleName)
    {
       
        if($avatarDir = NULL){
            
        }
        $fs = new Filesystem();
        $fs->remove($this->getTargetDir() . $this->defaultStoreDir. $FleName);

    }
         public function removeAvatar($FleName)
    {
       
             //var_dump($FleName);
            // die();
        $fs = new Filesystem();
        $fs->remove($this->getAvatarDir() . $this->defaultStoreDir. $FleName);

    }
     public function filePath($file, $baseurl, $defaultImage)
    {

        $paths = $defaultImage;
        if($file != NULL){
           $paths =$file->getImagePath().$file->getAvatar().'';
        }
       return $Path = $baseurl . $paths;
    }

    public function getTargetDir()
    {
        return $this->targetDir;
    }
    public function getAvatarDir()
    {
        return $this->avatarDir;
    }
    
}