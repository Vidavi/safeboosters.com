<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Controller\InitController;
use Symfony\Component\HttpFoundation\Session\Session;


class NewsController extends Controller implements InitController
{
    /**
     * @Route("/news", name="news")
     */
    public function newsAction(Request $request)
    {

        if (false === $this->get('security.authorization_checker')->isGranted('ROLE_MEMBER')) {
          $this->get('session')->getFlashBag()->add('error', 'Only members are enable to visit this page.');
          return $this->redirectToRoute("shop");
        }
       	$em = $this->get('doctrine')->getManager();
        $news = $em->getRepository('AppBundle:News')->news();
        $repository = $em->getRepository('AppBundle:Product');

        $helper = $this->get('vich_uploader.templating.helper.uploader_helper');
        $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();

        foreach($news as &$new){

            $userId = $new['u_id'];
            $product = $repository->findOneBy(array('userId' => $userId));
               
            if($product === NULL){
                     $paths="/image/web/manager.png";
            }else{
                     $paths = $helper->asset($product, 'imageFile');
            }

            $avatarPath = $baseurl . $paths;
            $new["path"] = $avatarPath;
 
           	switch($new["n_message_type"]){

           		case 	1:
           			$string = $new['t_story'];
           			break;
           		case 	2:
           			$string = $new['p_post_content'];
           			break;
           		case 	3:
           			$string = "JOB";
           			break;
           	}
       
             $content = strip_tags($string);
 			 $new['content_length'] = strlen($content);

			 if (strlen($content) > 100) {
			 
			    $stringCut = substr($content, 0, 100);
			    $content = substr($stringCut, 0, strrpos($stringCut, ' ')); 
			   
			 }

			 $new["content"] = $content;
			       
		}

        return $this->render('news/news.html.twig', array('news'=>$news));
    }

    
}
