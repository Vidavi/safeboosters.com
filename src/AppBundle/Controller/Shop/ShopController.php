<?php

namespace AppBundle\Controller\Shop;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Controller\InitController;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use AppBundle\Entity\Topic;
use AppBundle\Entity\IPN_PDT;
use AppBundle\Entity\Paypal;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ShopController extends Controller implements InitController
{
    /**
     * @Route("/shop", name="shop")
     */
    public function shopAction(Request $request)
    {
        

        $standard = $this->getParameter('standard');
        $business = $this->getParameter('business');


          
        return $this->render('shop/shop.html.twig', array('standard'=>$standard, 'business'=>$business));
    }
    /**
     * @Route("/thank-you", name="thankYou")
     */
    public function thankYouAction(Request $request)
    {


        file_put_contents('pdt.txt', file_get_contents('php://input'));
        $pp_hostname = "www.paypal.com"; // Change to www.sandbox.paypal.com to test against sandbox
        // read the post from PayPal system and add 'cmd'
        $req = 'cmd=_notify-synch';
        $tx_token = $_GET['tx'];
        $auth_token = "xxx";
        $req .= "&tx=$tx_token&at=$auth_token";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://$pp_hostname/cgi-bin/webscr");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        //set cacert.pem verisign certificate path in curl using 'CURLOPT_CAINFO' field here,
        //if your server does not bundled with default verisign certificates.
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: $pp_hostname"));
        $res = curl_exec($ch);
        curl_close($ch);


        if(!$res){
                die('die!');
        }else{
            // parse the data
            $lines = explode("\n", $res);
            $keyarray = array();
            
            if (strcmp ($lines[0], "SUCCESS") == 0) {
        
        
                for ($i=1; $i<count($lines);$i++){
                        list($key,$val) = explode("=", $lines[$i]);
                        $keyarray[urldecode($key)] = urldecode($val);
                }
    
                $item_name        = $keyarray['item_name'];
                $payment_status   = $keyarray['payment_status'];
                $payment_amount   = $keyarray['mc_gross'];
                $payment_currency = $keyarray['mc_currency'];
                $txn_id           = $keyarray['txn_id']; 
                $receiver_email   = $keyarray['receiver_email']; 
                $payer_email      = $keyarray['payer_email'];

                


            // warnings
            if ($receiver_email != $this->getParameter('paypalSellerEmail')) $description[] = "Incorrect seller e-mail";  
            if ($payment_status != 'Completed') $description[] = "payment_status != Completed"; 
            if (empty($description)) $description[] = "Payment completed";

            $txn_id = isset($txn_id) && !empty($txn_id) ? $txn_id : NULL;

            // Custom field from paypal
            if(isset($keyarray['custom'])){
                $custom_values = explode('|',$keyarray['custom']);
            }

            // save IPN/PDT
            $em = $this->get('doctrine')->getManager();
            $IPN_PDT = new IPN_PDT();
            $IPN_PDT->setType('PDT');
            $IPN_PDT->setDescription(serialize($description));
            $IPN_PDT->setTxnId($txn_id);
            $IPN_PDT->setPaymentStatus($payment_status);
            $IPN_PDT->setDetails(serialize($_GET));
            

            $em->persist($IPN_PDT);
            $em->flush();
           
             // check price 
            $standard = $this->getParameter('standard');
            $business = $this->getParameter('business');

            switch ($payment_amount) {
                case $standard['price']:
                    $days = "+ " .$standard['days'];
                    break;
                case $business['price']:
                    $days =  "+ " .$business['days'];
                    break;
                default:
                    $action = "Warning";
                    $desciption = "PDT: price doesnt match (mc_gross='".$payment_amount."|txn_id: '".$txn_id."'|parent_txn_id: '".$parent_txn_id."'.)";
                    $helper = $this->get('helper.functions');
                    $helper-> log(null, $action, $desciption);

                    die();
            }
            // check payment
            if($payment_status != "Completed"){ // Completed
                    $action = "Warning";
                    $desciption = "PDT: payment is not Completed (payment_status='".$payment_status."|txn_id: '".$txn_id."'|parent_txn_id: '".$parent_txn_id."'.)";
                    $helper = $this->get('helper.functions');
                    $helper-> log(null, $action, $desciption);

                    die();
            }
            // check parent_txn_id and txn_id
            $thisTxn_id = $em->getRepository('AppBundle:Paypal')->txn_id($txn_id);
            if($thisTxn_id != NULL){
                     $action = "Warning";
                     $desciption = "IPN: txn_id already exist! (payment_status='".$payment_status."|txn_id: '".$txn_id."'|parent_txn_id: '".$parent_txn_id."'.)";
                     $helper = $this->get('helper.functions');
                     $helper-> log(null, $action, $desciption);
 
                     return $this->render('shop/thankYou.html.twig');
            }
            // payment Completed save payment
            $Paypal = new Paypal();
            $Paypal->setType('PDT');
            $Paypal->setDescription(serialize($description));
            $Paypal->setTxnId($txn_id);
            $Paypal->setParentTxnId($parent_txn_id);
            $Paypal->setPaymentStatus($payment_status);
            $Paypal->setDetails(serialize($_GET));
            
            $em->persist($Paypal);
            $em->flush();
 
            //  update subscription
            $userId = $custom_values[0];

            $user = $em->getRepository('AppBundle:User')->getUSerById($userId);

            $endDate = $user->getSubEndDate();
            $start_date = new \DateTime('now');
            if($start_date > $endDate){
               
                $start_date->modify($days);
                $user->setSubEndDate($start_date);
            }else{
                $endDate->modify($days);
                $user->setSubEndDate($endDate);
            }

            $em->persist($user);
            $em->flush();

            $action = "Payment";
            $desciption = "PDT: New payment (payment_status='".$payment_status."|txn_id: '".$txn_id."'|parent_txn_id: '".$parent_txn_id."'|userId: '".$userId."'| days: '".$days."'.)";
            $helper = $this->get('helper.functions');
            $helper-> log($userId, $action, $desciption);
            
            // send email thank you!
           

    
            }else if (strcmp ($lines[0], "FAIL") == 0) {
                // $this->baseClass->msg->add('e', 'Something goes wrong, please contact with manager.');
                // $this->router->redirect('page/index');
            }
        }
    
        return $this->render('shop/thankYou.html.twig', array('standard'=>$standard, 'business'=>$business));
    }
    /**
     * @Route("/paypal", name="paypalButton")
     */
    public function paypalAction(Request $request)
    {

    	if (false === $this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
    			$this->get('session')->getFlashBag()->add('error', 'You must be logged in');
                return $this->redirectToRoute('login');
        }

    	$option = $request->get('option', NULL);
    	$amount = $request->get('amount', NULL);
        $paypalSellerEmail = $this->getParameter('paypalSellerEmail');

    	if($option === NULL || empty($option) || $amount === NULL || empty($amount)){
    		$this->get('session')->getFlashBag()->add('error', 'Invalid form');
            return $this->redirectToRoute('shop');
    	}


 		echo "<style>.preloader-wrapp {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
  background: #000; /*#080325*/
  z-index: 100000000; }
  .preloader-wrapp .preloader {
    position: absolute;
    top: 50%;
    left: 50%;
    margin-top: -100px;
    margin-left: -100px; }

.preloader-wrapp > img {
  position: absolute;
  bottom: 50%;
  left: 50%;
  width: 320px; /*180px;*/
  margin-left: -160px; /*90px;*/
  margin-bottom: -10px;
  z-index: 2; }

.preloader {
  position: relative;
  box-sizing: border-box;
  background-clip: padding-box;
  width: 200px;
  height: 200px;
  border-radius: 100px;
  border: 5px solid rgba(255, 255, 255, 0.1);
  -webkit-mask: linear-gradient(rgba(0, 0, 0, 0.1), black 90%);
  -webkit-transform-origin: 50% 60%;
      -ms-transform-origin: 50% 60%;
       -o-transform-origin: 50% 60%;
          transform-origin: 50% 60%;
  -webkit-transform: perspective(200px) rotateX(80deg);
          transform: perspective(200px) rotateX(80deg); }

.preloader:before,
.preloader:after {
    content: '';
  position: absolute;
  left: 0;
  top: 0;
  margin: -5px;
  box-sizing: inherit;
  width: inherit;
  height: inherit;
  border-radius: inherit;
  border: 5px solid rgba(255, 255, 255, 0.1);
  opacity: .85;
  border-color: transparent;
  border-top-color: #D92B4C;
  -webkit-animation: youplay-preloader-spin 1s infinite;
       -o-animation: youplay-preloader-spin 1s infinite;
          animation: youplay-preloader-spin 1s infinite; }

.preloader:after {
  -webkit-animation-delay: 0.35s;
       -o-animation-delay: 0.35s;
          animation-delay: 0.35s; }

@-webkit-keyframes youplay-preloader-spin {
  100% {
    -webkit-transform: rotate(360deg);
            transform: rotate(360deg); } }

@-o-keyframes youplay-preloader-spin {
  100% {
    -o-transform: rotate(360deg);
       transform: rotate(360deg); } }

@keyframes youplay-preloader-spin {
  100% {
    -webkit-transform: rotate(360deg);
         -o-transform: rotate(360deg);
            transform: rotate(360deg); } }

.preloader-wrapp > img,
.preloader-wrapp > .preloader {
  -webkit-animation: youplay-preloader-fade .5s;
       -o-animation: youplay-preloader-fade .5s;
          animation: youplay-preloader-fade .5s; }

@-webkit-keyframes youplay-preloader-fade {
  0% {
    opacity: 0; }
  100% {
    opacity: 1; } }

@-o-keyframes youplay-preloader-fade {
  0% {
    opacity: 0; }
  100% {
    opacity: 1; } }

@keyframes youplay-preloader-fade {
  0% {
    opacity: 0; }
  100% {
    opacity: 1; } }</style>";
		echo '<div class="page-preloader preloader-wrapp">
            <img src="../image/logo.png" alt="Jobs for boosters">
            <div class="preloader"></div>
        </div>';


       		echo '<form name="form" method="post" action="https://www.paypal.com/cgi-bin/webscr">';
            echo '<input type="hidden" name="cmd" value="_xclick">';
             echo '<input type="hidden" name="custom" value="'.$this->getUser()->getId().'">';
            echo '
            <input type="hidden" name="quantity" value="1">
            <input type="hidden" name="no_note" value="1">
            <input type="hidden" name="item_name" value="boosting service">
            <input type="hidden" name="currency_code" value="EUR">
            <input type="hidden" name="amount" value="'.$amount.'">
           	<input type="hidden" name="business" value="olgakrk7@gmail.com">
            <input type="hidden" name="lc" value="US"> 
            <input type="hidden" name="address_override" value="1">
            <input type="hidden" name="notify_url" value="https://safeboosters.com/IPN">
            <input type="hidden" name="return" value="https://safeboosters.com/thank-you">
            <input type="hidden" name="cancel_return" value="https://safeboosters.com/">';  
            echo '</form>';
            echo "
            <SCRIPT LANGUAGE='JavaScript'>
            document.form.submit();
            </SCRIPT>
            ";

         
        return 1;

        
    }

    /**
     * @Route("/IPN", name="IPN")
     */
    public function IPNAction(Request $request)
    {
    	
        file_put_contents('test.txt', file_get_contents('php://input'));
        define("DEBUG", 0);
        define("USE_SANDBOX", 0);
        define("LOG_FILE", "./ipn.log");

        // Read POST data 
        // reading posted data directly from $_POST causes serialization
        // issues with array data in POST. Reading raw POST data from input stream instead.
        $raw_post_data = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
            $keyval = explode ('=', $keyval);
            if (count($keyval) == 2)
                $myPost[$keyval[0]] = urldecode($keyval[1]);
        }
        // read the post from PayPal system and add 'cmd'
        $req = 'cmd=_notify-validate';
        if(function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        }
        foreach ($myPost as $key => $value) {
            if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            }else{
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }
        // Post IPN data back to PayPal to validate the IPN data is genuine
        // Without this step anyone can fake IPN data
            if(USE_SANDBOX == true) {
                $paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
            } else {
                $paypal_url = "https://www.paypal.com/cgi-bin/webscr";
            }
        $ch = curl_init($paypal_url);
            if ($ch == FALSE) {
              return FALSE;
            }

        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        if(DEBUG == true) {
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
        }

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
        $res = curl_exec($ch);
            if (curl_errno($ch) != 0) // cURL error
            {   
                if(DEBUG == true) { 
                    error_log(date('[Y-m-d H:i e] '). "Can't connect to PayPal to validate IPN message: " . curl_error($ch) . PHP_EOL, 3, LOG_FILE);
                }
                curl_close($ch);
                exit;
            }else{
                // Log the entire HTTP response if debug is switched on.
                if(DEBUG == true) {
                  error_log(date('[Y-m-d H:i e] '). "HTTP request of validation request:". curl_getinfo($ch, CURLINFO_HEADER_OUT) ." for IPN payload: $req" . PHP_EOL, 3, LOG_FILE);
                  error_log(date('[Y-m-d H:i e] '). "HTTP response of validation request: $res" . PHP_EOL, 3, LOG_FILE);
                }
                curl_close($ch);
            }
    
        $item_name        = $_POST['item_name'];
        $payment_status   = $_POST['payment_status'];
        $payment_amount   = $_POST['mc_gross'];
        $payment_currency = $_POST['mc_currency'];
        $txn_id           = $_POST['txn_id']; 
        $receiver_email   = $_POST['receiver_email']; 
        $payer_email      = $_POST['payer_email'];
        if(isset($_POST['parent_txn_id']) && !empty($_POST['parent_txn_id']))
            $parent_txn_id = $_POST['parent_txn_id'];

        $tokens = explode("\r\n\r\n", trim($res));
        $res = trim(end($tokens));


        if (strcmp ($res, "VERIFIED") == 0) {

            // warnings
            if ($receiver_email != $this->getParameter('paypalSellerEmail')) $description[] = "Incorrect seller e-mail";  
            if ($payment_status != 'Completed') $description[] = "payment_status != Completed"; 
            if (empty($description)) $description[] = "Payment completed";

            $txn_id = isset($txn_id) && !empty($txn_id) ? $txn_id : NULL;
            $parent_txn_id = isset($parent_txn_id) && !empty($parent_txn_id) ? $parent_txn_id : NULL;

            // Custom field from paypal
            if(isset($_POST['custom'])){
                $custom_values = explode('|',$_POST['custom']);
            }

            // save IPN/PDT
            $em = $this->get('doctrine')->getManager();
            $IPN_PDT = new IPN_PDT();
            $IPN_PDT->setType('IPN');
            $IPN_PDT->setDescription(serialize($description));
            $IPN_PDT->setTxnId($txn_id);
            $IPN_PDT->setParentTxnId($parent_txn_id);
            $IPN_PDT->setPaymentStatus($payment_status);
            $IPN_PDT->setDetails(serialize($_POST));
            

            $em->persist($IPN_PDT);
            $em->flush();
            
            // check price 
            $standard = $this->getParameter('standard');
            $business = $this->getParameter('business');

            switch ($payment_amount) {
                case $standard['price']:
                    $days = "+ " .$standard['days'];
                    break;
                case $business['price']:
                    $days =  "+ " .$business['days'];
                    break;
                default:
                    $action = "Warning";
                    $desciption = "IPN: price doesnt match (mc_gross='".$payment_amount."|txn_id: '".$txn_id."'|parent_txn_id: '".$parent_txn_id."'.)";
                    $helper = $this->get('helper.functions');
                    $helper-> log(null, $action, $desciption);

                    die();
            }
            // check payment
            if($payment_status != "Completed"){ // Completed
                    $action = "Warning";
                    $desciption = "IPN: payment is not Completed (payment_status='".$payment_status."|txn_id: '".$txn_id."'|parent_txn_id: '".$parent_txn_id."'.)";
                    $helper = $this->get('helper.functions');
                    $helper-> log(null, $action, $desciption);

                    die();
            }
            // check parent_txn_id and txn_id
            $thisTxn_id = $em->getRepository('AppBundle:Paypal')->txn_id($txn_id);
            if($thisTxn_id != NULL){
                     $action = "Warning";
                     $desciption = "IPN: txn_id already exist! (payment_status='".$payment_status."|txn_id: '".$txn_id."'|parent_txn_id: '".$parent_txn_id."'.)";
                     $helper = $this->get('helper.functions');
                     $helper-> log(null, $action, $desciption);
 
                     die();
            }
            // payment Completed save payment
            $Paypal = new Paypal();
            $Paypal->setType('IPN');
            $Paypal->setDescription(serialize($description));
            $Paypal->setTxnId($txn_id);
            $Paypal->setParentTxnId($parent_txn_id);
            $Paypal->setPaymentStatus($payment_status);
            $Paypal->setDetails(serialize($_POST));
            
            $em->persist($Paypal);
            $em->flush();
 
            //  update subscription
            $userId = $custom_values[0];
            $user = $em->getRepository('AppBundle:User')->getUSerById($userId);

            $endDate = $user->getSubEndDate();
            $start_date = new \DateTime('now');
            if($start_date > $endDate){
               
                $start_date->modify($days);
                $user->setSubEndDate($start_date);
            }else{
                $endDate->modify($days);
                $user->setSubEndDate($endDate);
            }

            $em->persist($user);
            $em->flush();

            $action = "Payment";
            $desciption = "IPN: New payment (payment_status='".$payment_status."|txn_id: '".$txn_id."'|parent_txn_id: '".$parent_txn_id."'|userId: '".$userId."'| days: '".$days."'.)";
            $helper = $this->get('helper.functions');
            $helper-> log($userId, $action, $desciption);
            
            // send email thank you!
             return $this->render('default/index.html.twig');

            if(DEBUG == true) {
              error_log(date('[Y-m-d H:i e] '). "Verified IPN: $req ". PHP_EOL, 3, LOG_FILE);
            }

        } else if (strcmp ($res, "INVALID") == 0) {
            // log for manual investigation
            // Add business logic here which deals with invalid IPN messages
            if(DEBUG == true) {
               error_log(date('[Y-m-d H:i e] '). "Invalid IPN: $req" . PHP_EOL, 3, LOG_FILE);
              }


          
        }  
        
    }
   
    
    
}