<?php

namespace AppBundle\Controller\User;
use AppBundle\Entity\Avatar;
use AppBundle\Entity\ChangePassword;
use AppBundle\Entity\user;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Controller\InitController;
use AppBundle\Form\AvatarType;
use AppBundle\Form\PasswordsType;
use AppBundle\Form\EmailsType;
use AppBundle\Form\UserType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserController extends Controller implements InitController
{

    /**
     * @Route("/user/profile", name="profile")
     */
    public function profileAction(Request $request)
    {
        

    $this->denyAccessUnlessGranted('ROLE_USER', null, 'Unable to access this page!');

      $em = $this->get('doctrine')->getManager(); 
      $user = $em->getRepository('AppBundle:User')->getUserById($this->getUser()->getId());

      $date1 = new \DateTime('now');
      $dateStart = $date1->getTimestamp();
      $date2 = $user->getSubEndDate();
      $dateEnd = $date2->getTimestamp();

      if($dateEnd > $dateStart){
        $clock = 1;
      }else{
        $clock = 0;
      }
     
    return $this->render('user/profile.html.twig',array('dateStart'=>$dateStart, 'dateEnd'=>$dateEnd, 'clock' =>$clock));
    }
    /**
     * @Route("user/activity", name="activity")
     */
    public function activityAction(Request $request)
    {
        
        $this->denyAccessUnlessGranted('ROLE_USER', null, 'Unable to access this page!');

      $em = $this->get('doctrine')->getManager(); 
      $user = $em->getRepository('AppBundle:User')->getUserById($this->getUser()->getId());

      $date1 = new \DateTime('now');
      $dateStart = $date1->getTimestamp();
      $date2 = $user->getSubEndDate();
      $dateEnd = $date2->getTimestamp();

      if($dateEnd > $dateStart){
        $clock = 1;
      }else{
        $clock = 0;
      }
        $news = $em->getRepository('AppBundle:News')->profileNews($this->getUser()->getId());

        foreach($news as &$new){
            switch($new["n_message_type"]){
              case  1:
                $string = $new['t_story'];
                break;
              case  2:
                $string = $new['p_post_content'];
                break;
            }
       
            $content = strip_tags($string);
            $new['content_length'] = strlen($content);

            if (strlen($content) > 100) {
          
                $stringCut = substr($content, 0, 100);
                $content = substr($stringCut, 0, strrpos($stringCut, ' ')); 
            }
            $new["content"] = $content;
        }

        return $this->render('user/activity.html.twig', array('news' =>$news, 'dateStart'=>$dateStart, 'dateEnd'=>$dateEnd, 'clock' =>$clock));
    }
     /**
     * @Route("user/settings", name="settings")
     */
    public function settingsAction(Request $request)
    {

        // Access
        $this->denyAccessUnlessGranted('ROLE_USER', null, 'Unable to access this page!');

        if (false === $this->get('security.authorization_checker')->isGranted('ROLE_USER')) { 
            $this->get('session')->getFlashBag()->add('error', 'Permission denied!');
            return $this->redirectToRoute('homepage'); 
        }

        $em = $this->get('doctrine')->getManager(); 
        $user = $em->getRepository('AppBundle:User')->getUserById($this->getUser()->getId());
  
        $date1 = new \DateTime('now');
        $dateStart = $date1->getTimestamp();
        $date2 = $user->getSubEndDate();
        $dateEnd = $date2->getTimestamp();
  
        if($dateEnd > $dateStart){
          $clock = 1;
        }else{
          $clock = 0;
        }

        $userId = $this->getUser()->getId();
        $userObj = $this->getUser();

        $user = new ChangePassword();
        $user->setUser($userObj);

        $changeEmail = $userObj;
       // var_dump($userId);
       // die();
        
        
       // if($imagen === NULL){
            
            $imagen = new Avatar();
           // $imagen->setUserId($userId);
        //}
      
   //var_dump($imagen);
     //       die('arigato');
        $avatarForm = $this->get('form.factory')->createNamedBuilder('avatarForm', AvatarType::class, $imagen)->getForm();
        $avatarForm->handleRequest($request); // AVATAR

        $userForm = $this->get('form.factory')->createNamedBuilder('user_form', PasswordsType::class, $user)->getForm();
        $userForm->handleRequest($request); // AVATAR

        $emailForm = $this->get('form.factory')->createNamedBuilder('email_form', EmailsType::class, $changeEmail)->getForm();
        $emailForm->handleRequest($request); // Email

        // AVATAR
        if ($avatarForm->isSubmitted() && $avatarForm->isValid()){
             $fileUploader = $helper = $this->get('AppBundle\Service\FileUploader');
          //  var_dump($imagen->getAvatar()->guessExtension());
            $checkImage = $em->getRepository('AppBundle:Avatar')->findOneBy(array('userId' => $userId));
       
            if($checkImage != null){

                $deleteImage = $fileUploader->removeAvatar($checkImage->getAvatar());
                 $em->remove($checkImage);
                $em->flush();
                
              
            }
           
            $Images = $fileUploader->uploadAvatar($imagen, $this->getUser()->getId());

            // = 'web/uploads/'.$Images->getImagePath().$Images->getAvatar().'';
            //$baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
            //$avatarPath = $baseurl . $paths;
            //
            //$session->set('user/avatar', $avatarPath);

            $this->get('session')->getFlashBag()->add('success', 'Avatar has been updated!');
            return $this->redirectToRoute('settings');
        }

        if ($userForm->isSubmitted() && $userForm->isValid()){

            $changePassword = $userForm->getData();

            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($changePassword->getUser());

            $password = $encoder->encodePassword($changePassword->getPlainPassword(), $changePassword->getUser()->getSalt());
            $changePassword->getUser()->setPassword($password);

            $em->persist($changePassword->getUser());
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Password has been updated!');
            return $this->redirectToRoute('settings');
    
        }

         if ($emailForm->isSubmitted() && $emailForm->isValid()){
           $changeEmail_data = $emailForm->getData();
           $em->persist($changeEmail_data);
            $em->flush();
       

            // NEW TOKEN\
        
            $oldToken = $this->get('security.token_storage')->getToken();
            // create the authentication token
            $token = new UsernamePasswordToken(
                $userObj, //user object with updated username
                null,
                $oldToken->getProviderKey(),
                $oldToken->getRoles());
            // update the token in the security context
            $this->container->get('security.token_storage')->setToken($token);
         }
 
      $userRrrors = array(); 

      foreach ($userForm->getErrors(true, true) as $error) {
          $userRrrors[] = $error->getMessage();
      }
      $emailRrrors = array(); 

      foreach ($emailForm->getErrors(true, true) as $error) {
          $emailRrrors[] = $error->getMessage();
      }

    $email_hash = preg_replace('/(?<=.).(?=.*@)/u','*',$this->getUser()->getEmail());
    


      return $this->render('user/settings.html.twig',array(
          'avatarForm' => $avatarForm->createView(),'user_form' => $userForm->createView(),'email_form' => $emailForm->createView(),  'user_errors' => $userRrrors,  'email_errors' => $emailRrrors, 'email_hash' => $email_hash, 'dateStart'=>$dateStart, 'dateEnd'=>$dateEnd, 'clock' =>$clock
        ));
    }

    /**
     * @Route("/register", name="register")
     */
    public function registerAction(Request $request)
    {

        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
          throw $this->createAccessDeniedException();
        }

        if(!$request->query->get('token')){          
          $this->get('session')->getFlashBag()->add('error', 'Registration is made by sending an ACTIVE key to the mailbox. Learn more in the Terms section or contact with us via email or skype');
          return $this->redirectToRoute('homepage');
        }

        $em = $this->getDoctrine()->getManager();
        $token = $request->query->get('token');
        // Create a new blank user and process the form
        $checkToken = $em->getRepository('AppBundle:User')->checkToken($token);

         if($checkToken === NULL){
           $this->get('session')->getFlashBag()->add('error', 'Invalid data');
           return $this->redirectToRoute('homepage');
         }
         if($checkToken->getStatus() != 'active'){
           $this->get('session')->getFlashBag()->add('error', 'Invalid activation key');
           return $this->redirectToRoute('homepage');
         }
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Encode the new users password
         
            $encoder = $this->get('security.password_encoder');
            $password = $encoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            $checkToken->setStatus('used');
            // Set their role
            $user->setRole('ROLE_USER');
            $user->setCreatedAt((new \DateTime('now')));

            // Save
            $em->persist($checkToken);
            $em->persist($user);
            $em->flush();

            $id = $user->getId();
            $action = "Register";
            $helper = $this->get('helper.functions');
            $helper-> log($id, $action);


            $this->get('session')->getFlashBag()->add('success', 'User has been created.');
            return $this->redirectToRoute("login");
        }

        return $this->render('auth/register.html.twig', [
            'form' => $form->createView(), 'token'=>$checkToken,
        ]);
    }

}

