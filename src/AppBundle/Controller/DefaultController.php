<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Controller\InitController;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use AppBundle\Entity\Topic;

class DefaultController extends Controller implements InitController
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
       
//    	echo "<pre>";
// var_dump($_SESSION);
// die('ok');

//       $em = $this->get('doctrine')->getManager();
//   $imagen = $em->getRepository('AppBundle:User')->findOneBy(array('id' => 3));
// 
//   $encoder = new MessageDigestPasswordEncoder('bcrypt');
// 
// 
// $password = $encoder->encodePassword($imagen->getPassword(), $imagen->getUser()->getSalt());
// 
// var_dump($password);
// die();
//  $session = $request->getSession();
//  $session->set('promo/code', 'my_code_value');
//  $session->set('promo/name', 'my_name_value');
// $ewr = $session->get('promo'); // returns 'my_code_value'
    //       $session = new Session();
    //       $session->set('tokens/c', 'wer');
    //        $session->set('tokens/a', 'wer');
    //        $all =  $session->all('tokns');
     //       
  //         $userAttributeBag = new AttributeBag('_dfg');
  //    
  //         $session->registerBag($userAttributeBag);
  //   //         
  //      $session->set('userId', '123hhtrhtr123');
  //      $session->set('userEmail',  '123rthhtrhtr123');
  //       $sessionr = $request->getSession();


   // echo '<pre>';
   // var_dump($_SESSION);
   // die();
      //  $user = $this->get('security.token_storage')->getToken()->getUser();
      // var_dump($user);
      // die();
      // $user = $this->get('session')->get('user');
 
    // $this->denyAccessUnlessGranted('ROLE_USER', null, 'Unable to access this page!');
   //if (false === $this->get('security.authorization_checker')->isGranted('role_user')) { die('qwe'); }

        return $this->render('default/index.html.twig');
    }
   
    /**
     * @Route("/search", name="search")
     */
    public function searchAction(Request $request)
    {

        $this->denyAccessUnlessGranted('ROLE_MEMBER', null, 'Unable to access this page!');

        $em = $this->get('doctrine')->getManager();
        $topics = $em->getRepository('AppBundle:Topic')->findAllTopic();
        
        return $this->render('search/quickSearch.html.twig', array('topicDatas'=> $topics));
    }
    /**
     * @Route("/terms", name="terms")
     */
    public function termsAction(Request $request)
    {
        return $this->render('default/terms.html.twig');
    }
    /**
     * @Route("/privacy", name="privacy")
     */
    public function privacyAction(Request $request)
    {
        return $this->render('default/privacy.html.twig');
    }
}
