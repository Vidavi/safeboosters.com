<?php

namespace AppBundle\Controller\admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Controller\InitController;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use AppBundle\Entity\Topic;
use AppBundle\Entity\RegisterToken;
use AppBundle\Form\UserType;
use AppBundle\Entity\User;
use AppBundle\Form\registerTokenType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;

class AdminController extends Controller implements InitController
{
    /**
     * @Route("/admin", name="adminHome")
     */
    public function adminHomeAction(Request $request)
    {
      if (false === $this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
          $this->get('session')->getFlashBag()->add('error', 'Only admins are enable to visit this page.');
          return $this->redirectToRoute("homepage");
        }
   

        return $this->render('admin/admin.html.twig');
    }
    /**
     * @Route("/topicStatusAdmin", name="topicStatusAdmin")
     */
    public function topicStatusAdminAction(Request $request)
    {
      if (false === $this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
          $this->get('session')->getFlashBag()->add('error', 'Only admins are enable to visit this page.');
          return $this->redirectToRoute("homepage");
        }
       
        $em = $this->get('doctrine')->getManager();
        $topics = $em->getRepository('AppBundle:Topic')->unverifiedTopic();
        
      
        foreach($topics as &$topic){
            $imagePath= array();
            $topicId = $topic['id'];
            $images = $em->getRepository('AppBundle:Image')->loadfilesById($topicId);
            foreach($images as $image){
              
                $imagePath[] = $image->getImagePath() . $image->getBrochure();
            }
            $topic['imagePath'] = $imagePath;
        }
        //echo "<prE>";
        //var_dump($topics);
        //die();
        return new JsonResponse($topics);
    }
    /**
     * @Route("/jobStatusAdmin", name="jobStatusAdmin")
     */
    public function jobStatusAdminAction(Request $request)
    {
      if (false === $this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
          $this->get('session')->getFlashBag()->add('error', 'Only admins are enable to visit this page.');
          return $this->redirectToRoute("homepage");
        }
       
        $em = $this->get('doctrine')->getManager();
        $jobs = $em->getRepository('AppBundle:Job')->unverifiedPJob();
        
      
        return new JsonResponse($jobs);
    }
    /**
     * @Route("/postStatusAdmin", name="postStatusAdmin")
     */
    public function postStatusAdminAction(Request $request)
    {
      if (false === $this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
          $this->get('session')->getFlashBag()->add('error', 'Only admins are enable to visit this page.');
          return $this->redirectToRoute("homepage");
        }
       
        $em = $this->get('doctrine')->getManager();
        $posts = $em->getRepository('AppBundle:Post')->unverifiedPost();
        
      
        
        return new JsonResponse($posts);
    }
    /**
    * @Route("/admin/logs", name="logs")
    */
    public function logsAction(Request $request)
    {
      if (false === $this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
          $this->get('session')->getFlashBag()->add('error', 'Only admins are enable to visit this page.');
          return $this->redirectToRoute("homepage");
        }
        $em = $this->get('doctrine')->getManager();
        
        $logs = $em->getRepository('AppBundle:Log')->lastlogs();
// $unverified = $em->getRepository('AppBundle:Log')->unverified();


        return $this->render('admin/log.html.twig', array('logs'=>$logs));
    }
    /**
    * @Route("/admin/registerToken", name="registerToken")
    */
    public function registerTokenAction(Request $request)
    {

          if (false === $this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
              $this->get('session')->getFlashBag()->add('error', 'Only admins are enable to visit this page.');
              return $this->redirectToRoute("homepage");
            }
          $em = $this->getDoctrine()->getManager();
           // Create a new blank user and process the form
          $userToken = new RegisterToken();
          $form = $this->createForm(registerTokenType::class, $userToken);
          $form->handleRequest($request);

          
          if($form->isSubmitted() && $request->isXmlHttpRequest()){
              $data = $form->getData();
              $checkEmail = $em->getRepository('AppBundle:User')->checkEmail($data->getEmail());
              if($checkEmail != NULL){
                  $form->get('email')->addError(new FormError('Sorry, this email address is already in use.'));
              }

              if ($form->isValid() ){
    
                $token = bin2hex(openssl_random_pseudo_bytes(64));

                  $userToken->setToken($token);
                  
                  $now = new \DateTime('now');
                  $date = $now->format('Y-m-d H:i:s');
                  $userToken->setCreatedAt($date);
                  $userToken->setStatus('active');

                  $em->persist($userToken);
                  $em->flush();
      
                  $id = $this->getUser()->getId();
                  $action = "TokenRegister";
                  $desciption = "Email: ".$userToken->getEmail()." Token: ".$userToken->getToken();
                  $helper = $this->get('helper.functions');
                  $helper-> log($id, $action, $desciption);
      
                  return new JsonResponse(array('return' => 1, 'msg'=>'User has been created.'));
              }
          

              $postErrors = array(); 

              foreach ($form->getErrors(true, true) as $error) {
                  $postErrors[] = $error->getMessage();        
              }
           
        
              return new JsonResponse(array('return' => 0, 'msg'=>$postErrors));
         }
         return $this->render('admin/registerToken.html.twig',['form' => $form->createView()]);
    }
    /**
     * @Route("/admin/register", name="registerByAdmin")
     */
    public function registerByAdminAction(Request $request)
    {

      if (false === $this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
          $this->get('session')->getFlashBag()->add('error', 'Only admins are enable to visit this page.');
          return $this->redirectToRoute("homepage");
        }

        // Create a new blank user and process the form
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Encode the new users password
            $encoder = $this->get('security.password_encoder');
            $password = $encoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            // Set their role
            $user->setRole('ROLE_USER');
            $user->setCreatedAt((new \DateTime('now')));
            $user->setSubEndDate((new \DateTime('now'))->modify('-1 hour'));

            // Save
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $id = $user->getId();
            $action = "Register";
            $helper = $this->get('helper.functions');
            $helper-> log($id, $action);


            $this->get('session')->getFlashBag()->add('success', 'User has been created.');
            return $this->redirectToRoute("registerByAdmin");
        }

        return $this->render('admin/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
    * @Route("/admin/tokens", name="tokens")
    */
    public function tokensAction(Request $request)
    {
      if (false === $this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
          $this->get('session')->getFlashBag()->add('error', 'Only admins are enable to visit this page.');
          return $this->redirectToRoute("homepage");
        }

        $em = $this->get('doctrine')->getManager();
        $tokens = $em->getRepository('AppBundle:RegisterToken')->getTokens();
       
        return new JsonResponse($tokens);
    }
    /**
     * @Route("/admin/user", name="adminUser")
     */
    public function adminUserAction(Request $request)
    {
      if (false === $this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
          $this->get('session')->getFlashBag()->add('error', 'Only admins are enable to visit this page.');
          return $this->redirectToRoute("homepage");
        }
     

        return $this->render('admin/users.html.twig');
        
    }
        /**
     * @Route("/admin/userJson", name="userJson")
     */
    public function userJsonAction(Request $request){
    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
          $this->get('session')->getFlashBag()->add('error', 'Only admins are enable to visit this page.');
          return $this->redirectToRoute("homepage");
        }
        
        $em = $this->get('doctrine')->getManager();
        $user = $em->getRepository('AppBundle:User')->getAllUsers();

    
        return new JsonResponse($user);
}
    
    /**
     * @Route("/topicCheckAdmin", name="topicCheckAdmin")
     */
    public function topicCheckAdminAction(Request $request)
    {
      if (false === $this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
          $this->get('session')->getFlashBag()->add('error', 'Only admins are enable to visit this page.');
          return $this->redirectToRoute("homepage");
        }
        if(!$request->isXmlHttpRequest()){
            die('die');
        }
        $topicId = $request->request->get('id');
 
        if($topicId == NULL){
            return new JsonResponse(array('return' => 0, 'msg'=>'Invalid post.'));
        }
        $em = $this->get('doctrine')->getManager();
        $topic = $em->getRepository('AppBundle:Topic')->findTopicById($topicId);
        $topic[0]->setStatus(true);
        $em->persist($topic[0]);
        $em->flush();
     
        return new JsonResponse(array('return' => 1, 'msg'=>'Checked.'));
    }
    /**
     * @Route("/postCheckAdmin", name="postCheckAdmin")
     */
    public function postCheckAdminAction(Request $request)
    {
      if (false === $this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
          $this->get('session')->getFlashBag()->add('error', 'Only admins are enable to visit this page.');
          return $this->redirectToRoute("homepage");
        }
        if(!$request->isXmlHttpRequest()){
            die('die');
        }
        $topicId = $request->request->get('id');
 
        if($topicId == NULL){
            return new JsonResponse(array('return' => 0, 'msg'=>'Invalid post.'));
        }
        $em = $this->get('doctrine')->getManager();
        $post = $em->getRepository('AppBundle:Post')->findPostById($topicId);
        $post->setStatus(true);
        $em->persist($post);
        $em->flush();
     
        return new JsonResponse(array('return' => 1, 'msg'=>'Checked.'));
    }
    /**
     * @Route("/jobCheckAdmin", name="jobCheckAdmin")
     */
    public function jobCheckAdminAction(Request $request)
    {
      if (false === $this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
          $this->get('session')->getFlashBag()->add('error', 'Only admins are enable to visit this page.');
          return $this->redirectToRoute("homepage");
        }
        if(!$request->isXmlHttpRequest()){
            die('die');
        }
        $topicId = $request->request->get('id');
 
        if($topicId == NULL){
            return new JsonResponse(array('return' => 0, 'msg'=>'Invalid post.'));
        }
      
        $em = $this->get('doctrine')->getManager();
        $post = $em->getRepository('AppBundle:Job')->jobByIdObject($topicId);
     
        $post->setStatus(true);
        $em->persist($post);
        $em->flush();
     
        return new JsonResponse(array('return' => 1, 'msg'=>'Checked.'));
    }
    
}
