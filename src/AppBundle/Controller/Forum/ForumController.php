<?php
namespace AppBundle\Controller\Forum;

use AppBundle\Entity\Product;
use AppBundle\Entity\ChangePassword;
use AppBundle\Entity\user;
use AppBundle\Entity\Post;
use AppBundle\Entity\Topic;
use AppBundle\Entity\News;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Controller\InitController;
use AppBundle\Form\AvatarType;
use AppBundle\Form\PasswordsType;
use AppBundle\Form\EmailsType;
use AppBundle\Form\PostType;
use AppBundle\Form\NewTopicType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Form\FormError;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;

class ForumController extends Controller implements InitController
{
    /**
     * @Route("/forum", name="forum")
     */
    public function forumAction(Request $request)
    {
        
    $this->denyAccessUnlessGranted('ROLE_MEMBER', null, 'unable to access this page!');
   //if (false === $this->get('security.authorization_checker')->isGranted('role_user')) { die('qwe'); }

        $em = $this->get('doctrine')->getManager();
        $categorys = $em->getRepository('AppBundle:Category')->findAll();
    
        $fileUploader = $helper = $this->get('AppBundle\Service\FileUploader');
        $defaultImage = $this->getParameter('defaultImage');
        $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
        
        foreach ($categorys as $category) {

            if($category->getLast_topic_by_user_id() != NULL){
                
                    $userId = $category->getLast_topic_by_user_id();
                    $checkAvatar = $em->getRepository('AppBundle:Avatar')->findOneBy(array('userId' => $userId));
                    $filePath = $fileUploader->filePath($checkAvatar, $baseurl, $defaultImage);
                    $category->setPath($filePath);
                    
             }else{
                    $category->setPath(NULL);
             }

        }

    return $this->render('Forum/category.html.twig', array('categorys' => $categorys));
    }
    /**
    * @Route("/forum/category/id/{cat_id}", name="topic")
    */
    public function topicAction(Request $request, $cat_id)
    {
        
    $this->denyAccessUnlessGranted('ROLE_MEMBER', null, 'unable to access this page!');
   //if (false === $this->get('security.authorization_checker')->isGranted('role_user')) { die('qwe'); }

        $em = $this->get('doctrine')->getManager();

        $category = $em->getRepository('AppBundle:Category')->findCategoryById($cat_id);
        if($category === NULL){
            throw new NotFoundHttpException("Page not found");
        }

        //  $topics = $em->getRepository('AppBundle:Topic')->findAllTopicById($category->getId());

        $fileUploader = $helper = $this->get('AppBundle\Service\FileUploader');
        $defaultImage = $this->getParameter('defaultImage');
        $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
        
        $dql = 'SELECT t FROM AppBundle:Topic t WHERE t.topic_cat = ?1 ORDER BY t.Created_at DESC'; 
        $query = $em->createQuery($dql); 
        $query->setParameter(1, $category->getId());
                 
        $paginator  = $this->get('knp_paginator');
        $results = $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1),
            5
            );
     
        foreach($results as $result){
            $userId = $result->getLastPostByUserId();     
            $product = $em->getRepository('AppBundle:Avatar')->findOneBy(array('userId' => $userId));
                   
            $filePath = $fileUploader->filePath($product, $baseurl, $defaultImage);
            $result->path = $filePath;
        }
         
        return $this->render('Forum/topic.html.twig', array('pagination' => $results, 'categoryName' => $category->getCat_name(), 'categoryId' => $cat_id));
    }
    /**
    * @Route("/forum/topic/{topic_id}", name="singleTopic")
    */
    public function singleTopicAction(Request $request, $topic_id)
    {

        $this->denyAccessUnlessGranted('ROLE_MEMBER', null, 'unable to access this page!');

        $em = $this->get('doctrine')->getManager(); 
        $thisTopic = $em->getRepository('AppBundle:Topic')->findTopicById($topic_id);
        
        // var_dump($thisTopic);die();
        if($thisTopic == NULL){
            throw new NotFoundHttpException("Page not found");
        }
         $files = $em->getRepository('AppBundle:Image')->loadfilesById($thisTopic[0]->getId());

        $fileUploader = $helper = $this->get('AppBundle\Service\FileUploader');
        $defaultImage = $this->getParameter('defaultImage');
        $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
          
        $userId = $thisTopic[0]->getTopicBy();
      
       $userData = $em->getRepository('AppBundle:Avatar')->findOneBy(array('userId' => $userId));
        $filePath = $fileUploader->filePath($userData, $baseurl, $defaultImage);
           
        $thisTopic[0]->path = $filePath;
               

        $postModel = new Post();
        $postForm = $this->get('form.factory')->createNamedBuilder('postForm', postType::class, $postModel)->getForm();
        $postForm->handleRequest($request); 

        if ($postForm->isSubmitted() && $postForm->isValid() ){

            $data = $postForm->getData();
            $user = $this->getUser();
            $data->setPostTopic($topic_id);
            $data->setPostBy($user->getId());
            $data->setUserName($user->getName());
            $data->setCreatedAt((new \DateTime('now')));
            
        
            
             try {
              $em->getConnection()->beginTransaction();

          
              $em->persist($data);
              $em->flush();

              // update topic
              $topic = $em->getRepository('AppBundle:Topic')->findTopicById($topic_id);
              
              
               $checkVoice = $em->getRepository('AppBundle:Post')->checkPostById($topic_id, $this->getUser()->getId());
              
              if($checkVoice === NUll){
                  $topic[0]->increaseVoicesCounter();
              }
             
              $topic[0]->increasePostCounter();
              $topic[0]->setLastPostByUserId($this->getUser()->getId());
              $topic[0]->setLastPostByUserName($this->getUser()->getName());
              $topic[0]->setLastPostDate($data->getCreatedAt());

              // update category
              $category = $em->getRepository('AppBundle:Category')->findCategoryById($topic[1]->getId());
              $category->increasePostCounter();
            
              $news = new News();
              $news->setMessageName('New post');
              $news->setUserId($this->getUser()->getId());
              $news->setTopicId($topic_id);
              $news->setMessageType(2);
              $news->setCreatedAt(new \DateTime('now'));
              $news->setPostId($data->getId());
              $em->persist($category);
              $em->persist($topic[0]);
              $em->persist($news);
              $em->flush();

              // log
              $action = "New post";
              $desciption = "New post by: ".$this->getUser()->getId();
              $helper = $this->get('helper.functions');
              $helper-> log($this->getUser()->getId(), $action, $desciption);

              $em->getConnection()->commit();
          } catch (\Exception $e) {
              $em->getConnection()->rollback();
              
              $action = "Error";
              $desciption = "Message: ".$e->getMessage()." New topic: ".$category->getCat_name()." by: ".$this->getUser()->getId();
              $helper = $this->get('helper.functions');
              $helper-> log($this->getUser()->getId(), $action, $desciption);

              $this->get('session')->getFlashBag()->add('error', 'Something goes wrong, please try again later.');
              return $this->redirectToRoute("homepage");
          }

            $this->get('session')->getFlashBag()->add('success', 'Post has been updated!');

            return $this->redirectToRoute("singleTopic", array('topic_id'=>$topic_id, 'topic_name'=> $thisTopic[0]->getTopicname(), 'categoryName'=> $thisTopic[0]->getCatName(), 'categoryId'=> $thisTopic[0]->getTopicCat(), 'topic' =>$thisTopic));
          
        }

        $topic = $em->getRepository('AppBundle:Topic')->findTopicById($topic_id);

        if(empty($topic)){
          throw new NotFoundHttpException("Page not found");
        }


        // search
        $info = array();

        if($topic[0]->getScamerName() != NULL){
           $info['scamer_name'] = $topic[0]->getScamerName();
        }
        if($topic[0]->getScammerIGN() != NULL){
           $info['scammer_ign'] = $topic[0]->getScammerIGN();
        }
        if($topic[0]->getScammerEmail() != NULL){
           $info['scammerEmail'] = $topic[0]->getScammerEmail();
        }
        if($topic[0]->getScammerSkype() != NULL){
           $info['scammer_skype'] = $topic[0]->getScammerSkype();
        }
        if($topic[0]->getScammerContactApp() != NULL){
           $info['scammer_contact_app'] = $topic[0]->getScammerContactApp();
        }


        $searchs = $em->getRepository('AppBundle:Topic')->search($info);



        $newSearch = array();
        foreach ($searchs as $search ) {
            if($search->getId() != $topic[0]->getId()){
                $newSearch[] = $search;
            }
        }

   

        //$posts = $em->getRepository('AppBundle:Post')->findAllPostById($topic[0]->getId());

        
        $dql = 'SELECT p FROM AppBundle:Post p WHERE p.post_topic = ?1'; 
        $query = $em->createQuery($dql); 
        $query->setParameter(1, $topic[0]->getId());
                 
        $paginator  = $this->get('knp_paginator');
        $results = $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1),
            5
            );
 
        foreach($results as $result){
            $userId = $result->getPostBy();  
            
            $product = $em->getRepository('AppBundle:Avatar')->findOneBy(array('userId' => $userId));       
            $filePath = $fileUploader->filePath($product, $baseurl, $defaultImage);
          
            $result->path = $filePath;
        }

        $postErrors = array(); 

        foreach ($postForm->getErrors(true, true) as $error) {
            $postErrors[] = $error->getMessage();        }
          
          // var_dump($newSearch[0]->getTopic_name());
          //   die();


        return $this->render('Forum/singleTopic.html.twig', array('pagination' => $results, 'topic_name' => $topic[0]->getTopicname(), 'categoryName' => $topic[1]->getCat_name(), 'categoryId' => $topic[1]->getId(), 'post_form' => $postForm->createView(),  'post_errors' => $postErrors,  'topic' =>$thisTopic, 'searches' => $newSearch, 'files'=>$files));
    }

    /**
     * @Route("/forum/newTopic/categoryId/{cat_id}", name="newTopic")
     */
    public function newTopicAction(Request $request, $cat_id)
    {
        
        $this->denyAccessUnlessGranted('ROLE_MEMBER', null, 'unable to access this page!');
        $em = $this->get('doctrine')->getManager();

        if(is_integer($cat_id)){
            $this->get('session')->getFlashBag()->add('error', 'ID must be integer');
             return $this->redirectToRoute("forum");
        }

        $category = $em->getRepository('AppBundle:Category')->findCategoryById($cat_id);
        if($category === NULL){
            throw new NotFoundHttpException("Page not found");
        }
   


        $newTopic = new Topic();
        $topicForm = $this->get('form.factory')->createNamedBuilder('newTopic', NewTopicType::class, $newTopic)->getForm();
      
         $topicForm->handleRequest($request); 

        if ($topicForm->isSubmitted() && $topicForm->isValid() ){

          if(empty($_POST['newTopic']['scamer_name']) && empty($_POST['newTopic']['scammer_IGN']) && empty($_POST['newTopic']['scammer_email']) && empty($_POST['newTopic']['scammer_skype']) && empty($_POST['newTopic']['scammer_contactApp'])){
        
         $this->get('session')->getFlashBag()->add('error', 'You have to provide atlaest one information about scammer');

         return $this->redirectToRoute("newTopic", array('cat_id'=>$cat_id));
      }
          // Topic
          $data = $topicForm->getData();
       
          $data->setTopicCat($cat_id);
          $data->setCatName($category->getCat_name());
          $data->setTopicBy($this->getUser()->getId());
          $data->setTopicByName($this->getUser()->getName());
          $data->increaseVoicesCounter();
            
            $now = new \DateTime('now');
            $date = $now->format('Y-m-d H:i:s');
            $data->setCreatedAt($date);
         
        $data->setLastPostByUserId($this->getUser()->getId());
        $data->setLastPostByUserName($this->getUser()->getName());
      
       $data->setLastPostDate($now);
        
          $files = $data->getBrochure($data);
          $data->setBrochure('');
      
      
            $em->getConnection()->beginTransaction();

            $em->persist($data);
            $em->flush();
        
            $fileUploader = $helper = $this->get('AppBundle\Service\FileUploader');
            $Images = $fileUploader->upload($files, $data->getId());

              // NEWS
              $news = new News();
              $news->setMessageName('New topic');
              $news->setUserId($this->getUser()->getId());
              $news->setTopicId($data->getId());
              $news->setMessageType(1);
              $news->setCreatedAt(new \DateTime('now'));

              // update category
              $category = $em->getRepository('AppBundle:Category')->findCategoryById($cat_id);
              $category->setLast_topic_by_user_id($this->getUser()->getId());
              $category->setLast_topic_by_user_name($this->getUser()->getName());
              $category->setLast_topic_date($data->getCreatedAt());
              $category->increaseTopicCounter();

              // log
              $action = "New topic";
              $desciption = "New topic: ".$category->getCat_name()." by: ".$this->getUser()->getId();
              $helper = $this->get('helper.functions');
              $helper-> log($this->getUser()->getId(), $action, $desciption);

              $em->persist($category);
              $em->persist($news);
              $em->flush();

              $em->getConnection()->commit();
         

          $this->get('session')->getFlashBag()->add('success', 'New topic has been created');
          return $this->redirectToRoute("singleTopic", array('topic_id'=>$data->getId()));
   
        }

        $topicErrors = array(); 
        foreach ($topicForm->getErrors(true, true) as $error) {
            $topicErrors[] = $error->getMessage();
        }
        return $this->render('Forum/newTopic.html.twig', array('newTopic' => $topicForm->createView(), 'topicErrors'=>$topicErrors));
    }

    /**
     * @Route("/editTopic/topicId/{topic_id}", name="editTopic")
     */
    public function editTopicAction(Request $request, $topic_id)
    {
        $this->denyAccessUnlessGranted('ROLE_MEMBER', null, 'unable to access this page!');
        $em = $this->get('doctrine')->getManager();
        
        $thisTopic = $em->getRepository('AppBundle:Topic')->findTopicById($topic_id);
        $imageFiles = $em->getRepository('AppBundle:Image')->loadfilesById($thisTopic[0]->getId());
    
        if($thisTopic === NULL){
            throw new NotFoundHttpException("Page not found");
        }

        if($this->getUser()->getId() != $thisTopic[0]->getTopicBy()){
          $this->get('session')->getFlashBag()->add('error', 'Only author is ablew to edit this topic.');
          return $this->redirectToRoute("newTopic", array('cat_id'=>$cat_id));
        }

        $topicForm = $this->get('form.factory')->createNamedBuilder('editTopic', NewTopicType::class, $thisTopic[0])->getForm();
        $topicForm->handleRequest($request);

        if ($topicForm->isSubmitted() && $topicForm->isValid() ){

            $data = $topicForm->getData();
     
        $data->setLastEditTime(new \DateTime('now'));
        $files = $data->getBrochure($data);
      
        $data->setBrochure('');

            try {
                $em->getConnection()->beginTransaction();
            
                $em->persist($data);
                $em->flush();
          
                $fileUploader = $helper = $this->get('AppBundle\Service\FileUploader');
                $Images = $fileUploader->upload($files, $data->getId());
            
                $em->getConnection()->commit();
            } catch (\Exception $e) {

                $em->getConnection()->rollback();
                throw $e;
            }

          $this->get('session')->getFlashBag()->add('success', 'Topic has been edited!');
          return $this->redirectToRoute("singleTopic", array('topic_id'=>$thisTopic[0]->getId()));

        } 

        $err = array(); 
        foreach ($topicForm->getErrors(true, true) as $error) {
            $err[] = $error->getMessage();        }

         return $this->render('Forum/editTopic.html.twig', array('editTopic' => $topicForm->createView(), 'topic' => $thisTopic[0], 'editTopicErrors' => $err, 'imageFiles' => $imageFiles));

    }

     /**
     * @Route("/editPost/postId/{post_id}", name="editPost")
     */
    public function editPostAction(Request $request, $post_id)
    {
        $this->denyAccessUnlessGranted('ROLE_MEMBER', null, 'unable to access this page!');
        $em = $this->get('doctrine')->getManager();
        
        $thisPost = $em->getRepository('AppBundle:Post')->findPostById($post_id);

        
        if($thisPost === NULL){
            throw new NotFoundHttpException("Page not found");
        }

        if($this->getUser()->getId() != $thisPost->getPostBy()){
          $this->get('session')->getFlashBag()->add('error', 'Only author is enable to edit this topic.');
          return $this->redirectToRoute("singleTopic", array('topic_id'=>$thisPost->getPostTopic()));
        }
        // echo "<pre>";
        // var_dump($thisPost);
        // die();

        $postForm = $this->get('form.factory')->createNamedBuilder('editPost', PostType::class, $thisPost)->getForm();
        $postForm->handleRequest($request);

        if ($postForm->isSubmitted() && $postForm->isValid() ){

            $data = $postForm->getData();

          $data->setLastEditTime(new \DateTime('now'));

          try {
              $em->getConnection()->beginTransaction();
          
              $em->persist($data);
              $em->flush();
        
              $em->getConnection()->commit();
          } catch (\Exception $e) {

              $em->getConnection()->rollback();
              throw $e;
          }

        $this->get('session')->getFlashBag()->add('success', 'Post has been edited!');
        return $this->redirectToRoute("singleTopic", array('topic_id'=>$thisPost->getPostTopic()));

        } 

        $err = array(); 
        foreach ($postForm->getErrors(true, true) as $error) {
            $err[] = $error->getMessage();        }

         return $this->render('Forum/editPost.html.twig', array('editPost' => $postForm->createView(), 'post' => $thisPost, 'post_error' => $err));

    }
    /**
     * @Route("/removeFiles", name="removeFiles")
     */
    public function removeFilesAtion(Request $request)
    {
        
    $this->denyAccessUnlessGranted('ROLE_MEMBER', null, 'unable to access this page!');
     if(!$request->isXmlHttpRequest()){
         die('die');
     }
     if(!$request->request->get('request')){
         return new JsonResponse(array('return' => 0, 'msg'=>'Something goes wrong, please try again later..'));
     }
     
    $em = $this->get('doctrine')->getManager();
     
    $fileId = $request->request->get('request');
    $image = $em->getRepository('AppBundle:Image')->getImageById($fileId);
    if($image === NULL){
        return new JsonResponse(array('return' => 0, 'msg'=>'Something goes wrong, please try again later..'));
    }
    
    $topic = $em->getRepository('AppBundle:Topic')->findTopicById($image->getFileId());
    if($topic === NULL){
        return new JsonResponse(array('return' => 0, 'msg'=>'Something goes wrong, please try again later..'));
    }
 
    if($topic[0]->getTopicBy() != $this->getUser()->getId()){
        return new JsonResponse(array('return' => 0, 'msg'=>'You are not creator of this topic.'));
    }

         $fileUploader = $helper = $this->get('AppBundle\Service\FileUploader');
            $Images = $fileUploader->remove($image->getBrochure());
   
        // remove
        $em->remove($image);
        $em->flush();
        return new JsonResponse(array('return' => 1, 'msg'=>'Image has been deleted'));

    }
    /**
     * @Route("/GetFilesJson", name="GetFilesJson")
     */
    public function GetFilesJsonAtion(Request $request)
    {
        
    $this->denyAccessUnlessGranted('ROLE_MEMBER', null, 'unable to access this page!');
     if(!$request->isXmlHttpRequest()){
         die('die');
     }
   
 $fileId = $request->request->get('id');

    $em = $this->get('doctrine')->getManager();
    // $t = $this->get('request')->request->get('data');
   
    $files = $em->getRepository('AppBundle:Image')->loadfilesByIdJson($fileId);
   

    
        return new JsonResponse($files);

    }
}