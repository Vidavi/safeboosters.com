<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Job;
use AppBundle\Entity\News;
use AppBundle\Entity\Contact;
use AppBundle\Form\ContactType;
use AppBundle\Form\JobLolType;
use AppBundle\Form\JobCSGOType;
use AppBundle\Form\JobOverwatchType;
use AppBundle\Form\jobHeartStoneType;
use AppBundle\Form\jobDotaType;
use AppBundle\Form\jobWoWType;
use AppBundle\Form\jobHotSType;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\JsonResponse;

class ContactController extends Controller implements InitController
{
    /**
     * @Route("/contact", name="contact")
     */
    public function contactAction(Request $request)
    {
        $em = $this->get('doctrine')->getManager(); 
        $helper = $this->get('helper.functions');
  
        // Create the form according to the FormType created previously.
        // And give the proper parameters
        $contact = new Contact();
        $form = $this->createForm('AppBundle\Form\ContactType',$contact,array(
            // To set the action use $this->generateUrl('route_identifier')
            'action' => $this->generateUrl('contact'),
            'method' => 'POST'
        ));

        if ($request->isMethod('POST')) {
            // Refill the fields in case the form is not valid.
            $form->handleRequest($request);

            if($form->isValid()){

                $contactData = $form->getData();
                $contactData->setCreatedAt(new \DateTime('now'));
    
                if($this->sendEmail($contactData)){
                    
                    $contactData->setStatus(1);
                    $action = "New contact";
                    $desciption = "Status: 1| Message: ".$contactData->getMessage();
                  
                    $this->get('session')->getFlashBag()->add('success', 'Message has been sent!');

                }else{
                    $contactData->setStatus(0);
                    $action = "Error";
                    $desciption = "Status: 0| Message: ".$contactData->getMessage();
                    
                    $this->get('session')->getFlashBag()->add('error', 'Something went wrong, please try again later!');  
                }
                
                
                
                $helper-> log($this->getUser()->getId(), $action, $desciption);
                $em->persist($contactData);
                $em->flush();
                return $this->redirectToRoute('contact');
            }
        }

      return $this->render('contact/contact.html.twig',array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/job-applications", name="job")
     */
    public function jobAction(Request $request)
    {   
        $em = $this->get('doctrine')->getManager();
        $jobModel = new Job();
        $jobRrrors = array();
       
        $lolJonform = $this->get('form.factory')->createNamedBuilder('form1', JobLolType::class, $jobModel)->getForm();
        $CSGOform = $this->get('form.factory')->createNamedBuilder('form2', jobCSGOType::class, $jobModel)->getForm();
        $Overwatchform = $this->get('form.factory')->createNamedBuilder('form3', jobOverwatchType::class, $jobModel)->getForm();
        $hearthStoneForm = $this->get('form.factory')->createNamedBuilder('form4', jobHeartStoneType::class, $jobModel)->getForm();
        $dotaForm = $this->get('form.factory')->createNamedBuilder('form5', jobDotaType::class, $jobModel)->getForm();
        $WoWForm = $this->get('form.factory')->createNamedBuilder('form6', jobWowType::class, $jobModel)->getForm();
        $hotSForm = $this->get('form.factory')->createNamedBuilder('form7', jobHotSType::class, $jobModel)->getForm();


            
        if ($request->isMethod('POST')) {

            $id = $this->get('session')->get('user/id'); 
            $action = "Job";
            $helper = $this->get('helper.functions');

            $lolJonform->handleRequest($request);
            $CSGOform->handleRequest($request);
            $Overwatchform->handleRequest($request);
            $hearthStoneForm->handleRequest($request);   
            $dotaForm->handleRequest($request); 
            $WoWForm->handleRequest($request); 
            $hotSForm->handleRequest($request); 
         
            if ($lolJonform->isSubmitted()){
                    if($lolJonform->isValid()){
                        $jobModel = $lolJonform->getData();
                        $this->newJob($jobModel);
                        $helper->log($id, $action);

                        $this->get('session')->getFlashBag()->add('success', 'Your job application has been sent! Expect an e-mail or Skype response.');
                        return $this->redirectToRoute('job');
                    }
                    foreach ($lolJonform->getErrors(true, true) as $error) {
                            $jobRrrors[] = $error->getMessage();
                    }
            } 
            elseif ($CSGOform->isSubmitted()){
               
                if($CSGOform->isValid()){
                    $CSGOform = $CSGOform->getData();
                    $this->newJob($jobModel);
                    $helper->log($id, $action);
  
                    $this->get('session')->getFlashBag()->add('success', 'Your job application has been sent! Expect an e-mail or Skype response.');
                    return $this->redirectToRoute('job');
                  
                }
              
                foreach ($CSGOform->getErrors(true, true) as $error) {
                    $jobRrrors[] = $error->getMessage();
                }
            }

        
            elseif ($Overwatchform->isSubmitted()){
                if($Overwatchform->isValid()){
                    $Overwatchform = $Overwatchform->getData();
                    $this->newJob($Overwatchform);
                    $helper->log($id, $action);
    
                    $this->get('session')->getFlashBag()->add('success', 'Your job application has been sent! Expect an e-mail or Skype response.');
                    return $this->redirectToRoute('job');
                }   
                 foreach ($Overwatchform->getErrors(true, true) as $error) {
                    $jobRrrors[] = $error->getMessage();
                }
            }
             elseif ($hearthStoneForm->isSubmitted()){
                if($hearthStoneForm->isValid()){
                    $hearthStoneForm = $hearthStoneForm->getData();
                    $this->newJob($hearthStoneForm);
                    $helper->log($id, $action);
    
                    $this->get('session')->getFlashBag()->add('success', 'Your job application has been sent! Expect an e-mail or Skype response.');
                    return $this->redirectToRoute('job');
                }
                foreach ($hearthStoneForm->getErrors(true, true) as $error) {
                    $jobRrrors[] = $error->getMessage();
                }
            }

            elseif ($dotaForm->isSubmitted()){
                if($dotaForm->isValid()){
                    $dotaForm = $dotaForm->getData();
                    $this->newJob($dotaForm);
                    $helper->log($id, $action);
        
                    $this->get('session')->getFlashBag()->add('success', 'Your job application has been sent! Expect an e-mail or Skype response.');
                    return $this->redirectToRoute('job');
                }
                foreach ($dotaForm->getErrors(true, true) as $error) {
                    $jobRrrors[] = $error->getMessage();
                }
                        
            }
            elseif ($WoWForm->isSubmitted()){
                    if($WoWForm->isValid()){
                        $WoWForm = $WoWForm->getData();
                        $this->newJob($WoWForm);
                        $helper->log($id, $action);
            
                        $this->get('session')->getFlashBag()->add('success', 'Your job application has been sent! Expect an e-mail or Skype response.');
                    return $this->redirectToRoute('job');
                    }
                    foreach ($WoWForm->getErrors(true, true) as $error) {
                        $jobRrrors[] = $error->getMessage();
                    }     
            }
            elseif ($hotSForm->isSubmitted()){

                if($hotSForm->isValid()){

                        $hotSForm = $hotSForm->getData();
                        $this->newJob($hotSForm);
                        $helper->log($id, $action);
            
                        $this->get('session')->getFlashBag()->add('success', 'Your job application has been sent! Expect an e-mail or Skype response.');
                    return $this->redirectToRoute('job');
                }
                foreach ($hotSForm->getErrors(true, true) as $error) {
                    $jobRrrors[] = $error->getMessage();
                }
                    
            }else{
                die('bum');
            }

        }    
        return $this->render('contact/job.html.twig',array('lolJonform' => $lolJonform->createView(), 'CSGOform' => $CSGOform->createView(), 'overwatchFrom' => $Overwatchform->createView(), 'dotaForm' => $dotaForm->createView(), 'hearthStoneForm' => $hearthStoneForm->createView(),'WoWForm' => $WoWForm->createView(),'hotSForm' => $hotSForm->createView(), 'jobRrrors' => $jobRrrors));
    }


    private function sendEmail($data){
       // var_dump($data);
       // die();
        $myappContactMail = 'safeboosters@gmail.com';
        $myappContactPassword = 'de6zKmpP7SHYQTfVu9xP';
        
       
        // In this case we'll use the ZOHO mail services.
        // If your service is another, then read the following article to know which smpt code to use and which port
        // http://ourcodeworld.com/articles/read/14/swiftmailer-send-mails-from-php-easily-and-effortlessly
        $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
            ->setUsername($myappContactMail)
            ->setPassword($myappContactPassword);

        $mailer = \Swift_Mailer::newInstance($transport);
        
        $message = \Swift_Message::newInstance("Contact from boostingscams.com")
        ->setFrom(array($myappContactMail => "Message by ".$data->getName()))
        ->setTo(array(
            $myappContactMail => $myappContactMail
        ))
        ->setBody($data->getMessage()."<br>ContactMail :".$data->getEmail());
      
         
      
     
        return $mailer->send($message);
    }

    /**
     * @Route("/job-list", name="job-list")
     */
    public function joblistAction(Request $request)
    {
        $em = $this->get('doctrine')->getManager();
        $type = $request->query->get('type');
        $jobType = $this->getParameter('jobType');
           
        if(isset($type)  && !empty($type) && in_array($type, $jobType)){
        
             $dql = 'SELECT j FROM AppBundle:Job j  WHERE j.type = ?1 ORDER BY j.id DESC'; 
             $query = $em->createQuery($dql);
             $query->setParameter(1, $type);
         
        }else{

             $dql = 'SELECT j FROM AppBundle:Job j ORDER BY j.id DESC'; 
             $query = $em->createQuery($dql);
        }

        $paginator  = $this->get('knp_paginator');
        $result = $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1),
            12
        );


      return $this->render('contact/job-list.html.twig',array('pagination' => $result
        ));
    }

     public function newJob($obj){

        $em = $this->get('doctrine')->getManager();

        try {
            $em->getConnection()->beginTransaction();
            $em->persist($obj);
            $em->flush();
            $news = new News();
            $news->setMessageName('New job application');
            $news->setJobId($obj->getId());
            $news->setMessageType(3);
            $news->setCreatedAt(new \DateTime('now'));

            $em->persist($news);
            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            throw $e;
        }

    }

    
     /**
     * @Route("/jobDetails", name="jobDetails")
     */
    public function jobDetailsAction(Request $request)
    {
        if (false === $this->get('security.authorization_checker')->isGranted('ROLE_MEMBER')) {
              return new JsonResponse(array('return' => 0, 'msg'=>'You are not a member.'));
        }
        if(!$request->isXmlHttpRequest()){
            return new JsonResponse(array('return' => 0, 'msg'=>'Invalid'));
        }
        
        $id = $request->get('id');
        if($id == NULL || empty($id)){
            return new JsonResponse(array('return' => 0, 'msg'=>'Invalid id'));
        }
        $em = $this->get('doctrine')->getManager();
        $jobDetails = $em->getRepository('AppBundle:Job')->jobById($id);
      
        return new JsonResponse(array('return'=> 1, 'response'=>$jobDetails));
    
    }
    
}
